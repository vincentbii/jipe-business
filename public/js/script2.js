$(function () {
            $('#financed_amount').DataTable({
                "processing": true,
        "serverSide": true,
        "ajax": "{{ url('/admin/financedAmount/') }}",
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'doc_name', name: 'doc_name'},
            {data: 'financed_amount', name: 'financed_amount'},
            {data: 'financed_note', name: 'financed_note'}
        ]
            });
        });