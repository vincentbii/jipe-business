<table class="table">
     <thead>
          <tr>
               <th>Name</th>
          </tr>
     </thead>
     <tbody>
     @foreach($counties as $key => $c)
     <tr class="id{{$c->id}}">
          <td>{{$c->name}}</td>
          <td>
          
               <a href="">Edit</a>
               <button value="{{$c->id}}" class="btn btn-danger btn-sm btn-dell">Delete</button>
          </td>
     </tr>
     @endforeach
     </tbody>
</table>