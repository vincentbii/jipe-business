@extends('frontend.layouts.app')
@section('content')
<div class="wrapper row3">
     <main class="hoc container clear">
          <div class="box box-success">   
               <div class="box-header with-border">
                    <h3 class="box-title">{{ 'Contact Us Form' }}</h3>
               </div>

               <div class="box-body">
                    <div class="table-responsive">
                         <h1>New Message</h1>
                         <ul>
                              <li>From: {{$data->name}}</li>
                              <li>Email: {{$data->email}}</li>
                              <li>Message: {{$data->message}}</li>
                         </ul>
                    </div>
               </div>
          </div>
     </main>
</div>
@endsection