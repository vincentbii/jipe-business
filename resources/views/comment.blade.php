<html>
     <head>
          
     </head>
     <body>
          <p>
               Hello {{$user->first_name}}&nbsp;&nbsp;{{$user->last_name}}. You have a new comment on your proposal. <br />
               Comment: {{$comment->comment}}<br />
               Comment By: {{$comment_by->first_name}}&nbsp;{{$comment_by->last_name}}
               Click <a href="{{url('upload')}}">here</a> to view the comment
          </p>
     </body>
</html>