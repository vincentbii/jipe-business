@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Update Education level</div>

                    <div class="panel-body">
                         <div class="form-group">
                              <div class="form-group">
                              {{ Form::model($education_level, ['method' => 'PATCH','url' => ['admin/education/update', $education_level->id]]) }}

                              {{Form::label('name', 'Education Level ID: ')}}
                              {{Form::text('id', null, ['class' => 'form-control'])}}

                              {{Form::label('name', 'Education Level Name: ')}}
                              {{Form::text('name', null, ['class' => 'form-control'])}}

                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection