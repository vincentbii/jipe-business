@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Education Levels</div>
                    <div class="panel-body">

                         <?php
                         if (count($education_level) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>Education Level ID</th>
                                             <th>Education Level Name</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($education_level as $education) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $education->id }}</td>
                                                  <td>{{ $education->name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'url' => ['admin/education/destroy', $education->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

                                                       |
                                                       <a href="{{ url('admin/education/edit', $education->id) }}" class="btn btn-primary">edit</a>
                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/education/create', 'Add new Level', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection