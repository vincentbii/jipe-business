@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="financed_amount" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ 'ID Number' }}</th>
                        <th>{{ 'Full Names' }}</th>
                        <th>{{ 'Proposal' }}</th>
                        <th>{{ 'Amount Financed' }}</th>
                        <th>{{ 'Note' }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}

    <script>
        $(function () {
            $('#financed_amount').DataTable({
                "processing": true,
        "serverSide": true,
        "ajax": "{{ route('admin.financedAmount') }}",
        "columns": [
            {data: 'id_number', name: 'id'},
            { "data": null , 
     "render" : function ( data, type, full ) { 
        return full['first_name']+' '+full['last_name'];}
      },
            {data: 'doc_name', name: 'doc_name'},
            {data: 'financed_amount', name: 'financed_amount'},
            {data: 'financed_note', name: 'financed_note'}
        ]
            });
        });
    </script>
@endsection
