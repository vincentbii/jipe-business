@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Set Default amount to be payed by the Client' }}</h3>
     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-hover">
                    <tr>
                         @if($amount == null)
                         {{Form::open(['url'=>'admin/amountpayable/store'])}}
                         <td>
                              {{Form::label('amount', 'Amount Payable')}}&nbsp;&nbsp;
                              {{Form::number('amount', null, ['class' => 'awesome'])}}&nbsp;&nbsp;
                              {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}
                         </td>
                         {{Form::close()}}
                         @else
                         {{Form::open(['url'=>'admin/amountpayable/update'])}}
                         <td>
                              {{Form::hidden('id', $amount->id)}}
                              {{Form::label('amount', 'Amount Payable')}}&nbsp;&nbsp;
                              {{Form::number('amount', $amount->amount, ['class' => 'awesome'])}}&nbsp;&nbsp;
                              {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}
                         </td>
                         {{Form::close()}}
                         @endif
                    </tr>
               </table>
          </div>
     </div>
</div><!--box box-success-->

@endsection