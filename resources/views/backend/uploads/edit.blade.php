@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Comment on the busines plan</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::model($business_plan, ['method' => 'PATCH','url' => ['admin/uploads/update', $business_plan->id]]) }}

                             
                              {{Form::hidden('id', null, ['class' => 'form-control'])}}
                              {{Form::label('doc_name', 'Comments on the document: ')}}
                              {{Form::textarea('comments', null, ['class' => 'form-control'])}}

                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection