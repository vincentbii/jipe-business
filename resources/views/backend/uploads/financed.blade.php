@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Business Uploads Management' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Business Uploads Management' }}</h3>
          <div class="box-tools pull-right">
                @include('backend.access.includes.partials.uploads-header-buttons')
            </div><!--box-tools pull-right-->
          

     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-condensed table-hover">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>National ID</th>
                              <th>Email</th>
                              <th>Business Plan Directory</th>
                              <th>Previewed</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <?php
                    foreach ($financed as $finance) {
                         ?>
                         <tbody>
                              <tr>
                                   <td>{{$finance->id}}</td>
                                   <td>{{$finance->first_name}}&nbsp;&nbsp;{{$finance->last_name}}</td>
                                   <td>{{$finance->id_number}}</td>
                                   <td>{{$finance->email}}</td>
                                   <td><a href="<?php echo $finance->doc_directory . '/' . $finance->doc_name; ?>">File Path</a></td>
                                   <td>
                                        <?php
                                        if ($finance->preview == TRUE) {
                                             ?>
                                             <input class="btn btn-success btn-xs" type="button" value="YES" disabled="disabled" />
                                             <?php
                                        } else {
                                             ?>
                                             <input class="btn btn-danger btn-xs" type="button" value="NO" disabled="disabled" />
                                             <?php
                                        }
                                        ?>
                                   </td>
                                   <td>
                                        {{ Form::open([ 
                                                       'method'  => 'delete',
                                                               'url' => [ 
                                                            'admin/uploads/destroy', $finance->id ] ,
                                                               'onsubmit' => "return confirm('Are you sure you want to delete?')",
                                                  ]) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                                        <a href="{{ url('admin/comments/index', $finance->id) }}" class="btn btn-primary btn-xs">View Comments</a>
                                        <a href="{{ url('admin/uploads/unfinance', $finance->id) }}" class="btn btn-primary btn-xs">Revert</a>
                                        <a href="{{ url('admin/uploads/view', $finance->id) }}" class="btn btn-info btn-xs">View</a>
                                        {{ Form::close() }}


                                   </td>
                              </tr>
                         </tbody>
                         <?php
                    }
                    ?>

               </table>
          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection
<script type="text/javascript">
     return confirm('Are you sure you want to delete?');
</script>