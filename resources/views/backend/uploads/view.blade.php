@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Business Uploads Management' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Business Uploads Management' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.uploads-header-buttons')
          </div><!--box-tools pull-right-->


     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-condensed table-hover">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Document Name</th>
                              <th>Requested Amount</th>
                              @if($details->financed == 'FALSE')
                              <th>Amount to finance</th>
                              <th>Note</th>
                              <th>Action</th>
                              @else
                              <th>Amount Financed</th>
                              <th>Note on Financing</th>
                              @endif
                              
                         </tr>
                    </thead>
                         <tbody>
                              <tr>
                                   <td>{{$details->id}}</td>
                                   <td>{{$details->doc_name}}</td>
                                   <td><?php echo 'KES. '.number_format($details->amount_to_be_financed); ?></td>
                                   @if($details->financed === 1)
                                   <td><?php echo 'KES. '.number_format($details->financed_amount); ?></td>
                                   <td>{{$details->financed_note}}</td>
                                   @else
                                   {{Form::open(['url' => 'admin/uploads/finance'])}}
                                   <td>
                                        {{Form::hidden('id',$details->id)}}
                                        
                                        {{Form::number('amount', null, ['class' => 'form-control input-sm'])}}
                                        
                                        
                                   </td>
                                   <td>
                                        {{Form::text('note', null, ['class' => 'form-control input-sm', 'required' => 'required'])}}
                                   </td>
                                   <td>
                                        {{Form::submit('Accept', ['class' => 'btn btn-success btn-sm', 'required' => 'required'])}}
                                        
                                        <!--<a class="btn btn-success btn-xs" href="{{url('admin/uploads/finance', $details->id)}}">Accept</a>-->
                                        
                                   </td>
                                   {{Form::close()}}
                                   @endif
                              </tr>
                         </tbody>

               </table>
          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection