@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Business Uploads Management' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Business Uploads Management' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.uploads-header-buttons')
          </div><!--box-tools pull-right-->
     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table class="table" id="financed_amount">
                    <thead>
                         <th>ID</th>
                    </thead>
                    @foreach($financedAmount as $item)
                    <tr class="item{{$item->id}}">
                    <td class="text-left">{{$item->id}}</td>
                    <td class="text-left">{{$item->financed_amount}}</td>
                    <td class="text-left">
                        
                    </td>
                </tr>
                    @endforeach
               </table>
               </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection
@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}

    <script>
        $(function () {
            $('#financed_amount').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.getFinancedAmount") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                },
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection