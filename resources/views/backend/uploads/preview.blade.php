@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Business Uploads Management' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Business Uploads Management' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.uploads-header-buttons')
          </div><!--box-tools pull-right-->


     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table class="table">
                                   <thead>
                                        <tr></tr>
                                   </thead>
                                   <tbody>
                                        <tr>
                                             <td>
                                                  {{ Form::model($file, ['method' => 'PATCH','url' => ['upload/update', $file->id]]) }}
                                                  {{Form::hidden('id', $file->id)}}
                                                  {{Form::textarea('description', $file->description, ['class'=>'form-control']) }}
                                                  
                                                  
                                                  {{Form::close()}}
                                             </td>
                                        </tr>
                                   </tbody>
                              </table>
          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection
<script type="text/javascript">

$(document).ready(function(){
     $('#').on('click'){
          alert('Hi')
     }
});

</script>