@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Business Uploads Management' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Business Uploads Management' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.uploads-header-buttons')
          </div><!--box-tools pull-right-->


     </div><!-- /.box-header -->
     <div class="box-body">
          <div class="table-responsive">
               <div id="load" style="position: relative;">
                    @foreach($articles as $article)
                    <div>
                         <h3>
                              <a href="{{ action('Backend\CommentsController@show', [$article->id]) }}">{{$article->comment }}</a>
                         </h3>
                    </div>
                    @endforeach
               </div>
          </div>
     </div>
</div>
@endsection