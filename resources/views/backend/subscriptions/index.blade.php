@extends('backend.layouts.app')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<div class="">
    <div id="manage-payment" class="box box-success">{{ 'Payments Dashboard' }}

        <div class="box-header with-border">
            <h3 class="box-title">{{ 'Payments Dashboard' }}</h3>
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.payments-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                @if (count($items) > 0)
                <section class="articles">
                    @include('backend.subscriptions.load')
                </section>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

<script type="text/javascript">

$(function () {
    $('body').on('click', '.pagination a', function (e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        $.ajax({
            url: url
        }).done(function (data) {
            $('.articles').html(data);
        }).fail(function () {
            alert('Subscriptions could not be loaded.');
        });
    }
});

</script>


