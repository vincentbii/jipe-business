<div id="load" style="position: relative;">
    <table id="roles-table" class="table table-condensed table-hover">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>ID Number</th>
            <th>Amount</th>
        </tr>
        

        @foreach($items as $item)
        <tr>
            <td>{{ $item->first_name }} {{ $item->last_name }}</td>
            <td><a href="mailto:{{ $item->email }}" target="_blank" >{{ $item->email }}</a></td>
            <td>{{ $item->id_number }}</td>
            <td><?php echo number_format($item->amount); ?></td>
        </tr>
        @endforeach
    </table>
    {{ $items->links() }}
</div>

