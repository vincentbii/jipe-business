@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Check Countries Of Investment</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::open(['route' => 'admin.countryofinvestmentstore']) }}
                              
                              @foreach($countries as $country)
                              
                              {{Form::label('country_id', $country->country_name, ['class'=>'checkbox-inline'])}}
                              {{ Form::checkbox('country_id[]', $country->id) }}
                              
                              @endforeach
                              
                              <hr />

                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection