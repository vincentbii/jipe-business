@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Countries Of Investment</div>
                    <div class="panel-body">

                         <?php
                         if (count($countriesofinvestment) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>Countries Of Investment</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($countriesofinvestment as $countryofinvestment) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $countryofinvestment->country_name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'route' => ['admin.countryofinvestmentdestroy', $countryofinvestment->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}

                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/countryofinvestment/create', 'Add new Country Of Investment', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection