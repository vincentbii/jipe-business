@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Set Default Affiliates Commission' }}</h3>
     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-hover">
                    <tr>
                         @if($affCommission == null)
                         {{Form::open(['route'=>'admin.settings.affCommission.store'])}}
                         <td>
                              {{Form::label('commission', 'Commission')}}&nbsp;&nbsp;
                              {{Form::number('commission', null, ['class' => 'awesome'])}}&nbsp;&nbsp;
                              {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}
                         </td>
                         {{Form::close()}}
                         @else
                         {{Form::model($affCommission, ['method' => 'PUT','route' => ['admin.settings.affCommission.update', $affCommission->id]])}}
                         <td>
                              {{Form::hidden('id', $affCommission->id)}}
                              {{Form::label('commission', 'Commission')}}&nbsp;&nbsp;
                              {{Form::number('commission', $affCommission->commission, ['class' => 'awesome'])}}&nbsp;&nbsp;
                              {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}
                         </td>
                         {{Form::close()}}
                         @endif
                    </tr>
               </table>
          </div>
     </div>
</div><!--box box-success-->

@endsection