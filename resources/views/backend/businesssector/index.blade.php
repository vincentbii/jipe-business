@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Business sectors</div>
                    <div class="panel-body">

                         <?php
                         if (count($business_sectors) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>Countries Of Investment</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($business_sectors as $business_sector) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $business_sector->name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'route' => ['admin.businesssector.destroy', $business_sector->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}

                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/businesssector/create', 'Add new Business Sector', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection