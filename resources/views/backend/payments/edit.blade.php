@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Update County</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::model($county, ['method' => 'PATCH','url' => ['admin/county/update', $county->id]]) }}

                              {{Form::label('name', 'County ID: ')}}
                              {{Form::text('id', null, ['class' => 'form-control'])}}

                              {{Form::label('name', 'County Name: ')}}
                              {{Form::text('name', null, ['class' => 'form-control'])}}

                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection