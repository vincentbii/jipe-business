@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="box box-success">{{ 'Payments Dashboard' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Payments Dashboard' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.payments-buttons')
          </div><!--box-tools pull-right-->


     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-condensed table-hover">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>National ID</th>
                              <th>Email</th>
                              <th>Amount</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach($payments as $payment)
                         <tr>
                              <td>{{$payment->user_id}}</td>
                              <td>{{$payment->first_name}}&nbsp;&nbsp;{{$payment->last_name}}</td>
                              <td>{{$payment->id_number}}</td>
                              <td>{{$payment->email}}</td>
                              <td><?php echo 'KES. ' .  number_format($payment->amount); ?></td>
                         </tr>
                         @endforeach
                         <tr>
                              <td colspan="4"><b>Total</b></td>
                              <td><b><?php echo 'KES. '.  number_format($sum); ?></b></td>
                         </tr>
                    </tbody>
               </table>
               
          </div>
     </div>
</div>
@endsection