@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.user-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Date Requested</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    
@endsection

@section('after-scripts')
    {{ Html::script("//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js") }}

    <script>
        $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{route('admin.reseller.CommWithdrawalRequest')}}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'mergeColumn', name: 'mergeColumn', searchable: false, sortable : false, visible:true},
            {data: 'created_at', name: 'created_at'},
            {
                data: null,
                className: "center",
                defaultContent: '<a href="" class="editor_edit">Edit</a> / <a href="" class="editor_remove">Delete</a>'
            }
        ]
    });
    </script>
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            alert("HU");
        }
    </script>
@endsection
