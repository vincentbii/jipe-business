@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="box box-success">{{ 'View Comments' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'View Comments' }}</h3>

     </div><!-- /.box-header -->
     @if (session('status'))
     <div class="alert alert-success">
          {{ session('status') }}
     </div>
     @endif

     <div class="box-body">
          <div class="table-responsive">
               @foreach($users as $user)
               <table id="roles-table" class="table table-condensed table-hover">
                    <thead>
                         <tr>
                              <th>Posted By</th>
                              <th>Comment</th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach($comments as $comment)
                         <tr>
                              <td>
                                   {{$commentor_id}}
                              </td>
                              <td>
                                   {{$comment}}
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
               <a href="{{ url('admin/comments/create', $user->id) }}" class="btn btn-primary btn-xs">Comment</a>
               @endforeach

          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection