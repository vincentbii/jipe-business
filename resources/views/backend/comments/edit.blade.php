@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Edit your comment</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::model($comment, ['method' => 'PATCH','url' => ['admin/comments/update', $comment->id]]) }}

                              {{Form::label('comment', 'Comment: ')}}
                              {{Form::text('comment', null, ['class' => 'form-control'])}}
                              <hr />
                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                         </ul>
                    </div>
                    @endif
               </div>
          </div>
     </div>
</div>
@endsection