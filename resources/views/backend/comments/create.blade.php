@extends('backend.layouts.app')
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif
@section('page-header')

<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="box box-success">{{ 'Make a Comment' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'View Comment' }}</h3>

     </div><!-- /.box-header -->

     <div class="box-body">
          <div class="table-responsive">
               {{Form::open(['url'=>'admin/comments/store'])}}
               {{Form::hidden('post_id',$proposal->id, ['class'=> 'form-control'])}}
               {{Form::hidden('user_id',$proposal->user_id, ['class'=> 'form-control'])}}
               {{Form::label('comment', 'Comment')}}
               {{Form::textarea('comment', null, ['class'=>'form-control'])}}
               <hr>
               {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
               {{Form::close()}}
          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection