@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="box box-success">{{ 'View Comments' }}
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'View Comments' }}</h3>

     </div><!-- /.box-header -->
     @if (session('status'))
     <div class="alert alert-success">
          {{ session('status') }}
     </div>
     @endif

     <div class="box-body">
          <div class="table-responsive">
               <!--@foreach($users as $user)-->
               <table id="roles-table" class="table table-condensed table-hover">
                    <thead>
                         <tr>
                              <th>Posted By</th>
                              <th>Comment</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach($comments as $comment)
                         <tr>
                              <td><?php
                                   echo $comment->first_name . ' ' . $comment->last_name;
                                   ?></td>
                              <td>{{$comment->comment}}</td>
                              <td>
                                   {{ Form::open(['method'  => 'delete', 'url' => [ 'comments/destroy', $comment->id ], 'onsubmit' => "return confirm('Are you sure you want to delete?')"]) }}
                                   <a class="btn btn-primary btn-xs" href="{{url('admin/comments/edit', $comment->id)}}">Edit</a>
                                   |
                                   {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                                   {{ Form::close() }}
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
               <a href="{{ url('admin/comments/create', $proposal->id) }}" class="btn btn-primary btn-xs">Comment</a>
               <!--@endforeach-->

          </div><!--table-responsive-->
     </div><!-- /.box-body -->
</div><!--box-->
@endsection