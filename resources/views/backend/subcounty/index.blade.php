@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Sub Counties</div>
                    <div class="panel-body">

                         <?php
                         if (count($sub_county) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>Sub County ID</th>
                                             <th>Sub County Name</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($sub_county as $sub_counties) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $sub_counties->id }}</td>
                                                  <td>{{ $sub_counties->name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'url' => ['admin/subcounty/destroy', $sub_counties->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

                                                       |
                                                       <a href="{{ url('admin/subcounty/edit', $sub_counties->id) }}" class="btn btn-primary">edit</a>
                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/subcounty/create', 'Add new Sub County', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection