@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Create Sub County</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::open(['url' => 'admin/subcounty/store']) }}
                              {{Form::label('county_id', 'County')}}
                              
                              <select name="county_id" class="form-control">
                                   <?php
                                   foreach ($counties as $county) {
                                        ?>
                                        <option value="<?php echo $county['id'] ?>" class="form-control"> <?php echo $county['name'] ?> </option>
                                        <?php
                                   }
                                   ?>

                              </select>
                              
                            
                              {{Form::label('name', 'Sub County Name: ')}}
                              {{Form::text('name', null, ['class' => 'form-control'])}}

                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection