@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div id="manage-payment" class="box box-success">{{ 'Payments Dashboard' }}

     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Payments Dashboard' }}</h3>
          <div class="box-tools pull-right">
               @include('backend.access.includes.partials.payments-buttons')
          </div><!--box-tools pull-right-->
     </div><!-- /.box-header -->

     <!-- Item Listing -->
     <div class="box-body">
          <div class="table-responsive">
               <table id="roles-table" class="table table-condensed table-hover">
                    <tr>
                         <th>Name</th>
                         <th>Email</th>
                         <th>ID Number</th>
                         <th>Amount</th>
                    </tr>
                    <tr v-for="item in items">
                         <td>@{{ item.first_name }}&nbsp;@{{ item.last_name }}</td>
                         <td><a href="mailto:@{{ item.email }}">@{{ item.email }}</a></td>
                         <td>@{{ item.id_number }}</td>
                         <td>@{{ item.amount }}</td>
                    </tr>
               </table>
          </div>

          <!-- Pagination -->
          <nav>
               <ul class="pagination">
                    <li v-if="pagination.current_page > 1">
                         <a href="#" aria-label="Previous"
                            @click.prevent="changePage(pagination.current_page - 1)">
                              <span aria-hidden="true">�</span>
                         </a>
                    </li>
                    <li v-for="page in pagesNumber"
                        v-bind:class="[ page == isActived ? 'active' : '']">
                         <a href="#"
                            @click.prevent="changePage(page)">@{{ page }}</a>
                    </li>
                    <li v-if="pagination.current_page < pagination.last_page">
                         <a href="#" aria-label="Next"
                            @click.prevent="changePage(pagination.current_page + 1)">
                              <span aria-hidden="true">�</span>
                         </a>
                    </li>
               </ul>
          </nav>
     </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

@endsection