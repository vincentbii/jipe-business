@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Methods of Payment</div>
                    <div class="panel-body">

                         <?php
                         if (count($payment_method) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>Method of Payment</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($payment_method as $paymentmethod) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $paymentmethod->name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'url' => ['admin/paymentmethod/destroy', $paymentmethod->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}

                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/paymentmethod/create', 'Add new Payment Method', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection