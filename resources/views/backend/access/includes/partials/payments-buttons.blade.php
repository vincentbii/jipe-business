<div class="pull-right mb-10 hidden-sm hidden-xs">
     <a href="{{ url('admin/subscriptions') }}" class="btn btn-success btn-xs">All Subscriptions</a>
     <a href="{{ url('admin/payments/inactive') }}" class="btn btn-info btn-xs">Inactive Subscriptions</a>
     <a href="{{ url('admin/payments/pending') }}" class="btn btn-warning btn-xs">Pending Subscriptions</a>
     <a href="{{ url('admin/payments/invalid') }}" class="btn btn-primary btn-xs">Invalid Subscriptions</a>
     <a href="{{ url('admin/payments/failed') }}" class="btn btn-danger btn-xs">Failed Subscriptions</a>
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
     <div class="btn-group">
          <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
               {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>
          </button>

          <ul class="dropdown-menu" role="menu">
               <li>{{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all')) }}</li>
               <li>{{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create')) }}</li>
               <li class="divider"></li>
               <li>{{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated')) }}</li>
               <li>{{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted')) }}</li>
          </ul>
     </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>