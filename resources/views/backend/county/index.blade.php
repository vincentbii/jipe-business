@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Counties</div>
                    <div class="panel-body">

                         <?php
                         if (count($counties) > 0) {
                              ?>
                              <table class="table" border="0">
                                   <thead>
                                        <tr>
                                             <th>County CODE</th>
                                             <th>County Name</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($counties as $county) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $county->id }}</td>
                                                  <td>{{ $county->name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'url' => ['admin/county/destroy', $county->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

                                                       |
                                                       <a href="{{ url('admin/county/edit', $county->id) }}" class="btn btn-primary">edit</a>
                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                   
                                   <?php
                              }
                              ?></table><?php
                         }
                         ?>
                         {{ link_to('admin/county/create', 'Add new County', ['class'=> 'btn btn-primary']) }}
                    </div>
                    
               </div>
               
          </div>
     </div>
</div>
@endsection