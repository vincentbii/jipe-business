@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Countries</div>
                    <div class="panel-body">
                         <table class="table" border="0">
                              <?php
                              if (count($countries) > 0) {
                                   ?>
                                   <thead>
                                        <tr>
                                             <th>Country ID</th>
                                             <th>Country Name</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($countries as $country) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $country->id }}</td>
                                                  <td>{{ $country->country_name }}</td>
                                                  <td>
                                                       {{ Form::open(['method' => 'DELETE', 'url' => ['admin/country/destroy', $country->id]]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                                       
                                                       |
                                                       <a href="{{ url('admin/country/edit', $country->id) }}" class="btn btn-primary">edit</a>
                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                        <?php
                                   }
                              }
                              ?>
                         </table>
                    </div>
               </div>
               {{ link_to('admin/country/create', 'Add new Country', ['class'=> 'btn btn-primary']) }}
          </div>
     </div>
</div>
@endsection