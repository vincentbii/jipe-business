@extends('backend.layouts.app')

@section('page-header')
<h1>
     {{ app_name() }}
     <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Create Country</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::open(['url' => 'admin/country/store']) }}
                            
                              {{Form::label('country_name', 'Country Name: ')}}
                              {{Form::text('country_name', null, ['class' => 'form-control'])}}
                              <hr />
                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection