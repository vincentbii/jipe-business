<!doctype html>
<html lang="{{ app()->getLocale() }}">
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="csrf-token" content="{{ csrf_token() }}">

          <title>@yield('title', app_name())</title>

          <!-- Meta -->
          <meta name="description" content="@yield('meta_description', 'Jipe Business Limited')">
          @yield('meta')

          <!-- Styles -->
          @yield('before-styles')

          <!-- Check if the language is set to RTL, so apply the RTL layouts -->
          <!-- Otherwise apply the normal LTR layouts -->
          @langRTL
          {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
          @else
          {{ Html::style(mix('css/frontend.css')) }}
          @endif

          @yield('after-styles')
          <!-- Scripts -->
          <script>
               window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
          </script>
          {{Html::style('layout/styles/layout.css')}}
          {{Html::style('layout/scripts/layout.js')}}

          


     </head>
     <body id="top">
          @include('includes.partials.logged-in-as')
          @include('frontend.includes.jeren_menu')
          @include('includes.partials.messages')
          @yield('content')
          <!-- Scripts -->
          @yield('before-scripts')
          {!! Html::script(mix('js/frontend.js')) !!}
          @yield('after-scripts')

          @include('includes.partials.ga')
          @include('frontend.includes.footer')
     </body>
</html>