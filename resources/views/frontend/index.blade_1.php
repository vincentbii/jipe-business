@extends('frontend.layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('/css/style.css') }}">
<div id="">
     <div class="highlight">
          <div class="clearfix">
               <div class="testimonial">
                    <!--<h2>Testimonials</h2>-->
                    <p>
                         <!--&ldquo;Aenean ullamcorper purus vitae nisl tristique sollicitudin. Quisque vestibulum, erat ornare.&rdquo;-->
                    </p>
                    <span></span>
               </div>
               <h1>Welcome to Jipe Business Limited</h1>
               <p>
                    We are about creating financial freedom and making dreams come true.
                    Join our fast-growing network of financially independent youths  and let your
                    money work for you.
                    You can become your own boss with only Ksh 1,000. 
               </p>
               <h1>Want To Be A Success Story? Start Now</h1>
               <p>
                    There are high level of unemployment in the country and the urge to become financially
                    independent and take charge of your own destiny is everyone's dream. It is becoming
                    increasingly imperative to take the risks that may open up a new world for you. 
                    At JIPE BUSINESS ltd we have launched products that will sooner rather than later will
                    make you be your own boss. JIPE BUSINESS is our latest product that will have young
                    poeple start their journey,
               </p>
               <p>
                    Thinking about starting your own business? JIPE business is here to help you get 
                    started to financial freedom.

               </p>
               <p>
                    Are you a school leaver or College/University graduate desperately looking for a job? 
                    Do you want to become the next millionaire from your home area by starting your own 
                    business? Our unique entrepreneurship support program seeks to grow entrepreneurs from
                    as little as Ksh 1,000. you are only required to submit your business proposal where u
                    are assured of funding (business grant) from ksh 5,000 upto Ksh 10,000,000 .
               </p>
          </div>
     </div>

     <div class="featured">
          <h2>Why Choose Us?</h2>
          <ul class="clearfix">
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/meeting.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our vision Statement</b> JIPE BUSINESS envisions a society where all people have access to business opportunities, skills, resources  they need to actualize their full  potentials.
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>
               </li>
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/meeting.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our Core Values</b> Respect and Honesty, Accountability and Integrity, Progress and Kindness
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>
               </li>
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/smile.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our Core Purpose</b> JIPE BUSINESS CORE PURPOSE IS TO EMPOWER AND GROW A NETWORK OF ENTREPRENUERS  TO IMPROVE THEIR LIVES AND THE LIVES OF OTHERS.
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>

               </li>
          </ul>

     </div>
</div>
@endsection