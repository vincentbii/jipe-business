@extends('frontend.layouts.app')

@section('content')
<div class="wrapper bgded overlay light" >
     <main class="hoc container clear">
          <div id="highlights">
               <div class="clearfix">

                    <div class="col-md-6 col-md-offset-3">

                         <div class="box-header with-border">
                              <h3 class="box-title">{{ 'Sign Up Form' }}</h3>
                         </div>
                         <div class="box-body">
                              <div class="">

                                   <table class="">
                                        <tbody>
                                             {{ Form::open(['route' => 'frontend.auth.register.post', 'class' => 'form-horizontal']) }}
                                             <tr>

                                                  @if($affcode !== null)
                                                  {{Form::hidden('affcode', $affcode)}}
                                                  @endif
                                                  
                                                  <td>{{ Form::text('first_name', null,['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}</td>
                                                  <td>{{ Form::text('last_name', null,
                            ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}</td>
                                             </tr>
                                             <tr>
                                                  <td colspan="2">{{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}</td>
                                             </tr>
                                             <tr>
                                                  <td>{{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}</td>
                                                  <td>{{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}</td>
                                             </tr>
                                             <tr>
                                                  <td>{{ Form::text('number', null,
                            ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Phone Number']) }}
                                                  </td>
                                                  <td>
                                                       {{ Form::number('id_number', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => 'ID Number']) }}
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td>
                                                       <select name="education_level" class="form-control">
                                                            <option value="">Select Education Level</option>
                                                            <?php
                                                            foreach ($education_level as $education) {
                                                                 ?>
                                                                 <option value="<?php echo $education['id'] ?>" class="form-control"> <?php echo $education['name'] ?> </option>
                                                                 <?php
                                                            }
                                                            ?>
                                                       </select>
                                                  </td>
                                                  <td>
                                                       <select name="country" class="form-control">
                                                            <option value="">Select Your Country</option>
                                                            <?php
                                                            foreach ($country as $countries) {
                                                                 ?>
                                                                 <option value="<?php echo $countries['id'] ?>" class="form-control"> <?php echo $countries['country_name'] ?> </option>
                                                                 <?php
                                                            }
                                                            ?>
                                                       </select>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td>
                                                       <select name="county" id="county" class="form-control">
                                                            <option value="">Select Your County</option>
                                                            <?php
                                                            foreach ($county as $counties) {
                                                                 ?>
                                                                 <option onchange="getValue()" value="<?php echo $counties['id'] ?>" class="form-control"> <?php echo $counties['name'] ?> </option>
                                                                 <?php
                                                            }
                                                            ?>
                                                       </select>
                                                  </td>
                                                  <td>
                                                       <select name="payment_method" id="payment_method" class="form-control">
                                                            <option value="">Select Payment Method</option>
                                                            <?php
                                                            foreach ($payment_method as $paymentmethod) {
                                                                 ?>
                                                                 <option value="<?php echo $paymentmethod->id ?>" class="form-control"> <?php echo $paymentmethod->name ?> </option>
                                                                 <?php
                                                            }
                                                            ?>
                                                       </select>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td colspan="2">
                                                       {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary btn-block']) }}
                                                  </td>
                                             </tr>
                                             {{ Form::close() }}
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>


               </div><!-- panel -->

          </div><!-- col-md-8 -->
     </main>
</div>

<script>
             $('#county').on('change', function(e){

     console.log(e);
             var county_id = e.target.value;
             $.get('/reg?county_id' + county_id, function(data){
             $('#subcounty').empty();
                     $.each(data, function(index, subCountyObj){
                     $('#subcounty').append('<option value="' + subCountyObj.id + '">' + subCountyObj.name + '</option>');
                     })
             })

     }){


     }
</script>
@endsection

@section('after-scripts')
@if (config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
@endsection