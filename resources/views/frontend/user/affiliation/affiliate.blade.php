@extends('frontend.layouts.app')
@section('content')
<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <section class="container" id="main-body">
     	<main class="hoc clear">
          <div class="row main-content">   
          	<div class="header-lined">
    <h1>Resellers <small>These statistics are in real time and update instantly</small></h1>

        <div class="row">
            <ul>
                <li>We pay commissions for every signup that comes via your custom signup link.</li>
                <li>We track the visitors you refer to us using cookies, so users you refer don't have to purchase instantly for you to receive your commission.  Cookies last for up to 90 days following the initial visit.</li>
                <li>If you would like to find out more, please contact us.</li>
            </ul>

            <div class="col-sm-4">
                <div class="affiliate-stat affiliate-stat-green alert-warning">
                    <i class="fa fa-users"></i>
                    <span>
                    {{$aff_clicks}}
                    </span>
                    <br>
                    Clicks
                </div>
            </div>

        <div class="col-sm-4">
            <div class="affiliate-stat affiliate-stat-green alert-info">
                <i class="fa fa-shopping-cart"></i>
                <span>
                    {{$aff_signups}}
                </span>
                <br>
                Signups
            </div>
        </div>

        <div class="col-sm-4" style="height: auto;">
            <div style="height: auto;" class="affiliate-stat affiliate-stat-green alert-success">
                <i class="fa fa-bar-chart-o"></i>
                <span>
                    {{$aff_conversion}}%
                </span>
                <br>
                Conversions
            </div>
        </div>
  </div>
    
<center>
	<h3>Your Unique Referral Link</h3>
	<input type="text" value="{{$affCode}}" class="form-control" id="affCode">
    <br>
<button class="btn btn-primary btn-sm" onclick="myFunction()">Copy To Clipboard</button>
</center>
<br>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-rounded">
                <!-- <tr>
                    <td class="text-right">Commissions Pending Maturation:</td>
                    <td><strong>KES0.00/=</strong></td>
                </tr> -->
                <tr>
                    <td class="text-right">Available Commision Balance</td>
                    <td>KES.&nbsp;{{$total_commission}}</td>
                </tr>
                <tr>
                    <td class="text-right">Total Amount Withdrawn</td>
                    <td>KES.&nbsp;
                        @if($commissionOne == null)
                        0.00
                        @else
                        {{$commissionOne->withdrawal}}
                        @endif
                    </td>
            </tr>
            </table>
        </div>
    </div>


        <p class="text-center alert alert-info">Get an immediate withdrawal of payouts on a request</p>
            <center>
                @if($comm_request == NULL)
                    {{Form::open(['route' => 'frontend.user.requestWith'])}}
                    {{Form::hidden('total_amount', $total_commission)}}
                    {{Form::submit('Request Withdrawal', ['class' => 'btn btn-success bt-sm'])}}
                    {{Form::close()}}
                @else
                    {{Form::open(['route' => 'frontend.user.requestWith'])}}
                    {{Form::hidden('total_amount', $total_commission)}}
                    {{Form::submit('Request Withdrawal', ['class' => 'btn btn-success bt-sm', 'disabled' => 'disabled'])}}
                    {{Form::close()}}
                @endif
                
            </center>
     	</div>
     </main>
     </section>
</div>
@endsection
@section('after-scripts')

<script type="text/javascript">
	function myFunction() {
  var copyText = document.getElementById("affCode");
  copyText.select();
  document.execCommand("Copy");
  alert("Copied the text: " + copyText.value);
}
</script>

<style type="text/css">
  .affiliate-stat {
    margin: 0 auto;
    padding: 15px;
    font-size: 1.6em;
    text-align: center;
    padding-bottom: 40px;
}

.affiliate-stat span {
    display: block;
    font-size: 2.0em;
}

.affiliate-stat i {
    float: left;
    padding: 10px;
    font-size: 4em;
}

@media (max-width: 1200px) {
    .affiliate-stat {
        font-size: 1.2em;
    }
    .affiliate-stat span {
        font-size: 2.1em;
    }
    .affiliate-stat i {
        font-size: 3.6em;
    }
}

@media (max-width: 992px) {
    .affiliate-stat {
        font-size: 1.0em;
    }
    .affiliate-stat span {
        font-size: 1.6em;
    }
    .affiliate-stat i {
        font-size: 3em;
    }
}
</style>

@endsection