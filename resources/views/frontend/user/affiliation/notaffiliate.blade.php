@extends('frontend.layouts.app')
@section('content')
<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <main class="hoc container clear">
          <div class="box box-success">   

<div class="header-lined">
    <h1>Activate Reseller Account</h1>
</div>
               <div class="box-body">
                    <div class="table-responsive">
                    	<div class="alert alert-info text-center">
            <h2>Get Paid for Referring Customers to Us</h2>
        Activate your reseller account and start earning money today...<br /><br />
                      </div>

                      <ul>
        <li>We pay commissions for every signup that comes via your custom signup link.</li>
        <li>We track the visitors you refer to us using cookies, so users you refer don't have to purchase instantly for you to receive your commission.  Cookies last for up to 90 days following the initial visit.</li>
        <li>If you would like to find out more, please contact us.</li>
    </ul>

    <br />

    <center>
      {{Form::open(['route' => 'frontend.user.activateReseller'])}}
    {{Form::submit('Activate Reseller Account', ['class' => 'btn btn-success'])}}
    {{Form::close()}}
    </center>
                    </div>
                </div>
     	</div>
     </main>
</div>
@endsection