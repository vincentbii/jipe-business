@extends('frontend.layouts.app')

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
<div class="wrapper row3">
     <main class=""> 
          <div class="container">
               <div class="row">
                    <div class="col-sm-3">
                         <div class="panel panel-default">
                              <div class="panel-heading">
                                   <h3 class="panel-title">My Profile</h3>
                              </div>
                              <h4 class="panel-body">
                                   {{ $logged_in_user->name }}<br />
                                   <small>
                                        {{ $logged_in_user->email }}<br />
                                        Joined {{ $logged_in_user->created_at->format('F jS, Y') }}
                                   </small>
                              </h4>
                              <div class="panel-footer">
                                   {{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => 'btn btn-info btn-sm btn-block']) }}

                                   @permission('view-backend')
                                   <hr />
                                   <center>
                                        {{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration'), [], ['class' => 'btn btn-danger btn-xs']) }}
                                   </center>
                                   @endauth
                              </div>
                         </div>
                    </div>
                    <div class="col-sm-9">
                         <div class="row">
                              <div class="col-sm-6">
                                   <div class="panel panel-default">
                                        <div class="panel-heading">
                                             <h3 class="panel-title">
                                                  Payments
                                             </h3>
                                        </div>
                                        <div class="panel-body">
                                             <?php
                                             header("Refresh:0; url=dashboard1");
                                             if ($amountPaid >= $amountPayable->amount) {
                                                  $extraAmount = $amountPaid - $amountPayable->amount;
                                                  if ($extraAmount > 0) {
                                                       ?><p>You have an overpayment of <?php echo '<b>KES. ' . number_format($extraAmount) . '</b>'; ?></p><?php
                                                  }
                                                  ?>
                                                  <p>Upload your business proposal</p>
                                                  <?php
                                             } elseif ($amountPaid < $amountPayable->amount) {
                                                  $amount_to_pay = $amountPayable->amount - $amountPaid;
                                                  ?>
                                                  <p>Your account reads <?php echo '<b>KES. ' . number_format($amountPaid) . '</b>'; ?>. Kindly pay another <?php echo '<b>KES. ' . number_format($amount_to_pay) . '</b>'; ?> to upload your proposal</p>
                                                  <p>{{link_to_route('frontend.user.makepayment', 'Make Payment',[], ['class' => 'btn btn-primary btn-sm'])}}</p>
                                                  <?php
                                             }
                                             ?>

                                        </div>
                                   </div>
                                   <div class="panel panel-default">
                                        <div class="panel-heading">
                                             <h3 class="panel-title">
                                                  Comments
                                             </h3>
                                        </div>
                                        @if($comments !== null && count($proposals) > 0)
                                        <div class="panel-body">
                                             You have {{$comments}} unread comment(s). Check the comments page to review
                                        </div>
                                        <div class="panel-footer">
                                             {{link_to_route('frontend.user.upload', 'View Comments', [], ['class' => 'btn btn-primary btn-xs'])}}
                                        </div>
                                        @endif
                                   </div>
                              </div>
                              <div class="col-sm-6">
                                   <div class="panel panel-default">
                                        <div class="panel-heading">
                                             <h3 class="panel-title">
                                                  Business Proposal
                                             </h3>
                                        </div>
                                        <div class="panel-body">
                                             <?php
                                             if ($amountPaid >= $amountPayable->amount) {
                                                  ?>
                                                  <p>Upload your business plan</p>
                                                  <?php
                                                  if (count($proposals) > 0) {
                                                       ?><p>Preview your uploaded proposals</p><?php
                                                  }
                                             } elseif ($amountPaid < $amountPayable->amount) {
                                                  if (count($proposals) > 0) {
                                                       ?><p>Preview your uploaded proposals</p><?php
                                                  }
                                             }
                                             ?>
                                        </div>
                                        <div class="panel-footer">
                                             <center>
                                                  <?php
                                             if ($amountPaid >= $amountPayable->amount) {
                                                  ?>
                                                  {{link_to_route('frontend.user.upload', 'Upload your proposal', [], ['class' => 'btn btn-primary btn-sm'])}}
                                                  <?php
                                                  if (count($proposals) > 0) {
                                                       ?>{{link_to_route('frontend.user.upload', 'Preview you upload proposals', [], ['class' => 'btn btn-primary btn-sm'])}}<?php
                                                  }
                                             } elseif ($amountPaid < $amountPayable->amount) {
                                                  if (count($proposals) > 0) {
                                                       ?>{{link_to_route('frontend.user.upload', 'Preview you upload proposals', [], ['class' => 'btn btn-primary btn-sm'])}}<?php
                                                  }
                                             }
                                             ?>
                                             </center>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </main>
</div>
@endsection
