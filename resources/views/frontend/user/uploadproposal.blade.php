
<div class="box box-success">   
     <div class="box-header with-border">
          <h3 class="box-title">{{ 'Upload a Business Plan' }}</h3>
     </div>

     <div class="box-body">
          <div class="table-responsive">

               <div class="">
                    <div class="">

                         {{ Form::open(['url'=>'store','files'=>true, 'class' => 'form-horizontal']) }}

                         <div class="col-sm-12">
                              <div class="form-group">
                                   <div class="col-sm-4 ">
                                        {{Form::label('business_sector', 'Business Sector')}}
                                        {{Form::select('business_sector', $business_sectors, ['class'=> 'form-control input-sm', 'required'=>'required'])}}
                                   </div><!--form-group-->

                                   <div class="col-sm-4">
                                        {{Form::label('amount_to_be_financed', 'Amount to be financed')}}
                                        {{Form::number('amount_to_be_financed', null, ['class'=>'awesome', 'required'=>'required'])}}
                                   </div><!--form-group-->
                                   
                                   <div class="col-sm-4">
                                       {{Form::label('amount_to_be_financed', 'Select Country Of Investment')}}
                                        <select name="country_id" class="">
                                             
                                             <?php
                                             foreach ($country_of_investment as $country) {
                                                  ?>
                                                  <option value="<?php echo $country->country_id ?>" class="form-control"> <?php echo $country->country_name ?> </option>
                                                  <?php
                                             }
                                             ?>
                                        </select>
                                   </div>
                              </div>



                              <div class="form-group">

                                   <div class="col-md-6">
                                        {{Form::label('description', 'Description', ['form-label'])}}<br />
                                        {{Form::textarea('description', null, ['rows' => 10, 'cols' => 100])}}
                                   </div><!--form-group-->
                              </div>

                              <div class="form-group">
                                   <div class="col-md-6">
                                        {{ Form::label('file','Upload Your File',array('id'=>'','class'=>'awesome')) }}
                                        {{ Form::file('file',null,array('id'=>'',['class'=>'form-control'])) }}

                                   </div><!--form-group-->
                              </div>



                              <div class="form-group">
                                   <div class="col-md-12 col-md-offset">
                                        <hr />
                                        {{ Form::submit('Submit', ['class'=>'btn btn-primary btn-sm']) }}

                                        <!-- reset buttons -->
                                        {{ Form::reset('Reset', ['class'=>'btn btn-info btn-sm']) }}

                                   </div><!--col-md-6-->
                              </div><!--form-group-->

                         </div>
                         {{ Form::close() }}
                    </div>

               </div>
          </div>

     </div><!-- panel body -->

</div><!-- panel -->