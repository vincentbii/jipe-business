@extends('frontend.layouts.app')

@section('content')
<div class="wrapper row3">
     <main class="hoc container clear">
          <div class="row">
               <div class="col-xs-12">
                    <div class="panel panel-default">
                         <div class="panel-heading">{{ 'Make Payment' }}</div>
                         <div class="panel-body">
                              <div class="row">
                                   {!! $iframe !!}
                              </div><!--row-->
                         </div><!--panel body-->
                    </div><!-- panel -->

               </div><!-- col-md-10 -->

          </div><!-- row -->
     </main>
</div>
@endsection
