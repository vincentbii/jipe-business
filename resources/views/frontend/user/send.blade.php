@extends('frontend.layouts.app')

@section('content')
<div class="row">

     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading">Business Plans</div>

               <div class="panel-body">

                    <div class="container-fluid">
                         <div class="row">
                              <div class="col-xs-6">
                                   <div class="panel panel-default">
                                        <div class="panel-heading">Upload a Business Plan</div>

                                        <div class="panel-body">

                                             <div class="container-fluid">
                                                  {{ Form::open(array('url'=>'upload/update','files'=>true)) }}
                                                  <div class="col-xs-6 col-md-4">
                                                       {{ Form::label('title','Title') }}
                                                       {{ Form::text('title',null,array('class'=>'form-control')) }}
                                                       
                                                       {{ Form::label('body','Body') }}
                                                       {{ Form::text('body',null,array('class'=>'form-control')) }}
                                                       <br/>
                                                       <!-- submit buttons -->
                                                       {{ Form::submit('Save', ['class'=>'btn btn-info']) }}


                                                  </div>
                                                  {{ Form::close() }}
                                             </div>
                                        </div>
                                   </div>

                              </div>
                         </div>
                         

                    </div><!--tab panel-->

               </div><!--panel body-->


          </div><!--panel body-->

     </div><!-- panel -->

</div><!-- col-xs-12 -->

</div><!-- row -->
@endsection