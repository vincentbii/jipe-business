@extends('frontend.layouts.app')

@section('content')
<div class="wrapper row3">
     <main class="hoc container">
          <div class="">
               <div class="box box-success">   
                    <div class="box-header with-border">
                         <h3 class="box-title">{{ $file->doc_name }}</h3>
                    </div>

                    <div class="box-body">
                         <div class="table-responsive">
                              <table class="table">
                                   <thead>
                                        <tr></tr>
                                   </thead>
                                   <tbody>
                                        <tr>
                                             <td>
                                                  {{ Form::model($file, ['method' => 'PATCH','url' => ['upload/update', $file->id]]) }}
                                                  {{Form::hidden('id', $file->id)}}
                                                  {{Form::textarea('description', $file->description, ['class'=>'form-control']) }}
                                                  <hr />
                                                  {{Form::submit('Update', ['class'=> 'btn btn-primary btn-sm'])}}
                                                  {{Form::close()}}
                                             </td>
                                        </tr>
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
     </main>
</div>
@endsection