@extends('frontend.layouts.app')
@section('content')
<div class="wrapper row3">
     <main class="hoc container clear">
          <div class="box box-success">   
               <div class="box-header with-border">
                    <h3 class="box-title">{{ 'Payment form' }}</h3>
               </div>
               <div class="box-body">
                    <div class="table-responsive">
                         <div class="col-sm-6">
                              {{Form::open(['route'=>'frontend.user.payment'])}}
                              <table class="table table-condensed">
                                   <tbody>
                                        <tr>
                                             <td>
                                                  {{Form::label('Amount')}}
                                                  {{Form::number('amount', $amount, ['class'=>'form-control input-sm'])}}
                                             </td>
                                             <td>
                                                  {{Form::label('User ID Number')}}
                                                  {{Form::number('id_number', $user->id_number, ['class' => 'form-control input-sm'])}}
                                             </td>
                                        </tr>
                                   </tbody>
                              </table>
                              <hr />
                              {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}
                              {{Form::close()}}
                         </div>
                         <div class="col-sm-6">

                         </div>
                    </div>
               </div>
          </div>
     </main>
</div>
@endsection