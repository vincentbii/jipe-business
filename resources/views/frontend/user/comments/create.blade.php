@extends('frontend.layouts.app')

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Create County</div>

                    <div class="panel-body">
                         <div class="form-group">
                              {{ Form::open(['url' => 'comments/store']) }}
                            
                              {{Form::label('comment', 'Comment: ')}}
                              {{Form::textarea('comment', null, ['class' => 'form-control'])}}
                              <hr>
                              {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                              {{ Form::close() }}
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
@endsection