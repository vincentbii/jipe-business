@extends('frontend.layouts.app')

@section('content')
<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                    <div class="panel-heading">Countries</div>
                    <div class="panel-body">
                         <table class="table" border="0">
                              <?php
                              if (count($comments) > 0) {
                                   ?>
                                   <thead>
                                        <tr>
                                             <th>Country ID</th>
                                             <th>Country Name</th>
                                             <th>Action</th>
                                        </tr>
                                   </thead>
                                   <?php
                                   foreach ($comments as $comment) {
                                        ?>
                                        <tbody>
                                             <tr>
                                                  <td>{{ $comment->id }}</td>
                                                  <td>{{ $comment->name }}</td>
                                                  <td>
                                                       {{ Form::open([
                                                                                          'method' => 'DELETE',
                                                                                                  'url' => [
                                                                                               'admin/country/destroy',
                                                                                               $comment->id
                                                                                          ],
                                                                                                  'onsubmit' => "return confirm('Are you sure you want to delete?')",
                                                                                     ]) }}
                                                       {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

                                                       |
                                                       <a href="{{ url('admin/country/edit', $comment->id) }}" class="btn btn-primary">edit</a>
                                                       {{ Form::close() }}
                                                  </td>
                                             </tr>
                                        </tbody>
                                        <a href="{{ url('comments/create', $comment->id) }}" class="btn btn-primary">Make a comment</a>
                                        <?php
                                   }
                              }
                              ?>
                         </table>
                    </div>
               </div>
               {{ link_to('comments/create', 'Make a comment', ['class'=> 'btn btn-primary']) }}

          </div>
     </div>
</div>
@endsection