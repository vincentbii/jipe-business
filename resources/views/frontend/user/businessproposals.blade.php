@extends('frontend.layouts.app')

@section('content')
@if (session('status'))
<div class="alert alert-success">
     {{ session('status') }}
</div>
@endif
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif
<div style="visibility: <?php echo $visibility; ?>" class="<?php echo $div; ?>">
     <div class="container">
          <div class="box box-success">   
               <div class="box-header with-border">
                    <h3 class="box-title">{{ 'Business Proposals' }}</h3>
               </div>

               <div class="box-body">
                    <div class="table-responsive">

                         <!-- Nav tabs -->
                         <ul class="nav nav-tabs" role="tablist">

                              @foreach($proposals as $count => $proposal)

                              <li role="presentation" @if($count == 0) class="active" @endif>
                                  <a href="#tab-{{ $proposal->id }}" aria-controls="#tab-{{ $proposal->id }}" role="tab" data-toggle="tab">{{ $proposal->doc_name }}</a
                              </li>

                              @endforeach 

                         </ul>

                         <!-- Tab panes -->
                         <div class="tab-content">

                              @foreach($proposals as $count =>$proposal)

                              <div role="tabpanel" @if($count == 0) class="tab-pane active" @else class="tab-pane" @endif id="tab-{{ $proposal->id }}">
                                   <table id="roles-table" class="table table-condensed table-hover">
                                        <thead>
                                             <tr>
                                                  <td>Doc Name</td>
                                                  <td>Business Sector</td>
                                                  <td>Amount to be Financed</td>
                                                  <td>Country Of Investment</td>
                                                  <td>Description</td>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td><a target="_blank" href="{{url('upload/download', $proposal->id)}}">{{$proposal->doc_name}}</a></td>
                                                  <td>{{$proposal->name}}</td>
                                                  <td><?php echo 'KES. ' . number_format($proposal->amount_to_be_financed); ?></td>
                                                  <td>{{$proposal->country_name}}</td>
                                                  <td>{{$proposal->description}}</td>

                                             </tr>
                                        </tbody>
                                   </table>
                                   @if($comments !== null)
                                   @foreach($comments as $comment)
                                   @if($comment->post_id == $proposal->id)
                                   @if ($comment->commentor_id == $comment->user_id)
                                   {{ Form::open([ 'method'  => 'delete', 'url' => [ 'comments/destroy', $comment->id],'onsubmit' => "return confirm('Are you sure you want to delete?')", ]) }}
                                   <div style="padding: 5px; background-color: lightblue; margin: 3px auto; width: 900px; float: right;">
                                        <p>{{$comment->comment}}</p>
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                                   </div>
                                   {{ Form::close() }}
                                   @else
                                   <div style="margin: 3px auto; float: left; width: 900px;">
                                        <span><i><b><small>{{$comment->first_name}}&nbsp;&nbsp;{{$comment->last_name}}</small></b></i></span>
                                        <div style="padding: 7px; background-color: skyblue; width: 100%; float: left;">
                                             <p>{{$comment->comment}}</p>
                                        </div>
                                   </div>
                                   @endif

                                   @endif

                                   @endforeach

                                   @endif

                                   {{Form::open(['route'=>'frontend.user.commentsstore'])}}
                                   {{Form::hidden('plan_id', $proposal->id)}}
                                   <hr />
                                   {{Form::textarea('comment', null, ['size' => '150x5']) }}
                                   <br />
                                   {{Form::submit('Comment', ['class' => 'btn btn-primary btn-xs'])}}

                                   {{Form::close()}}

                              </div>

                              @endforeach 

                         </div>

                    </div>

               </div>
          </div>
     </div>
</div>
@endsection