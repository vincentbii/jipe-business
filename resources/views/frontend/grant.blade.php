@extends('frontend.layouts.app')
@section('content')
<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <main class="hoc container clear"> 
          <!-- main body -->
          <!-- ################################################################################################ -->
          <div class="sidebar one_quarter first"> 
               <!-- ################################################################################################ -->
               <h6>JB Grant Program</h6>
               <nav class="sdb_holder">
                    <ul>
                         <li>Do you have what it takes to run your own small business?</li>
                         <li>Are you an individual or a group of young people wanting to start or grow your existing small business?</li>
                         <li>Then the JB Grant Programme is for you.</li>
                    </ul>
               </nav>
               <div class="sdb_holder">
                    <h6>JB Grant Program</h6>
                    <nav class="sdb_holder">
                         <ul>
                              <li>Individuals</li>
                              <li>Co-operatives</li>
                              <li>Community DevelopmentFacilitation Projects</li>
                         </ul>
                    </nav>
               </div>
               <!--############################################################################################## -->
          </div>
          <!-- ################################################################################################ -->
          <!-- ################################################################################################ -->
          <div class="content three_quarter">
               <h1>JB GRANT PROGRAMME</h1>
               <p>
                    The objective of the Grant Programme is to provide young entrepreneurs an opportunity to access both the financial and non-financial business development support to establish their survivalist businesses. The program is based in the principle of pulling recourses together .The programme focusses on youth entrepreneurs who are just coming into existence and beginning to display signs of future potential but not yet fully developed.
               </p>
               <h2>GRANT TYPES</h2>
               <ul>
                    <li><h3>Individuals</h3></li>
                    <p>
                         Formal and informal businesses grants will be issued to individuals who are at the promising and start-up phase oftheir development or development stage.
                    </p>
                    <li><h3>Co-operatives</h3></li>
                    <p>
                         Which are autonomous associations of persons united voluntarily to meet their common economic and social needsand aspirations through a jointlyowned and democratically controlled enterprise organised & operated on co-operative principles.
                    </p>
                    <li><h3>Community DevelopmentFacilitation Projects</h3></li>
                    <p>
                         The JB will also facilitate a process of community development through identifying projects that will be funded through JB funds and/or funds sourced from partners.
                    </p>
                    <p>
                         The types of businesses that will be assisted through the Grant Programme include but are not limited to; motormechanics/panel beaters, electricians, plumbers, domestic appliance repair services, beauticians, hair dressers,cleaning companies, small scale recycling companies, street vendors, car washes,shops, farming , education  and others.

                    </p>
               </ul>
               <h2>The qualifying criteria includes:</h2>
               <ul>
                    <li>The applicant must have attained the age of eighteen (18) years at the time of application</li>
                    <li>Require the grant for business start-up or growth</li>
                    <li>Are  with necessary skills, experience or; with the potential skill appropriate for the enterprise that they conduct or intend to conduct</li>
                    <li>Are KENYAN citizens and are resident within the borders of kenya</li>
                    <li>Are involved in the day-to-day operation and management of the business</li>
                    <li>Require grant from JB of not less than Ksh 3,000 and not more than Ksh 1,000,000</li>
                    <li>Operate either informally or formally; generally recognised as micro enterprises (e.g. street traders, vendors, emerging enterprises);</li>
                    <li>Have a profit motive and are commercially viable and sustainable</li>
                    <li>Members of entities should comprise 100% kenyan  citizen</li>
                    <li>Business are operating within the borders of kenya</li>
                    <li>Individual entity must have bank account and or a young person must be assisted to open an account; and</li>
                    <li>For cooperatives they must have or be willing to form a group of minimum 5 persons</li>
                    <li>Demonstrate a viable business idea</li>
                    <li>Have keen interest in establishing and running a business</li>
                    <li>Pay non refundable application fee of ksh 100 at the the time of application</li>
               </ul>
               <h2>Awarding Business Proposal</h2>
               <p>
                    Decisions on how and where funding is invested are made by a group of specialist within the organisation in a free and fair manner  to help ensure the money goes exactly where it is needed.
               </p>
               <!-- ################################################################################################ -->
          </div>
          <!-- ################################################################################################ -->
          <!-- / main body -->
          <div class="clear"></div>
     </main>
</div>
@endsection