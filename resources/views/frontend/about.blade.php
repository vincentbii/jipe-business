@extends('frontend.layouts.app')
@section('content')
<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <main class="hoc container clear">

          <div id="comments">
               <h2>About Jipe Business</h2>
               <ul>
                    <li>
                         <article>
                              <header><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
                                   <address>
                                        <a href="#">Our Vision Statement</a>
                                   </address>
                              </header>
                              <div class="comcont">
                                   <p>JIPE BUSINESS envisions a society where all people have access to business opportunities, skills, resources  they need to actualize their full  potentials.</p>
                              </div>
                         </article>
                    </li>
                    <li>
                         <article>
                              <header>
                                   <address>
                                        <a href="#">Core Values</a>
                                   </address>
                              </header>
                              <div class="comcont">
                                   <ul>
                                        <li>RESPECT AND HONESTY</li>
                                        JIPE BUSINESS believes all people have value and should be treated with dignity,honest and sincerity
                                        <li>Accountability and Integrity</li>
                                        JIPE BUSINESS is responsible to its participants, donors and other stakeholders.
                                        <li>Progress</li>
                                        JIPE BUSINESS believes in ongoing learning and innovation
                                        <li>Kindness</li>
                                        JIPE BUSINESS is a caring and generous organisation
                                   </ul>

                              </div>
                         </article>
                    </li>
                    <li>
                         <article>
                              <header>
                                   <address>
                                        <a href="#">Our Core Purpose</a>
                                   </address>
                              </header>
                              <div class="comcont">
                                   <p>JIPE BUSINESS CORE PURPOSE IS TO EMPOWER AND GROW A NETWORK OF ENTREPRENUERS  TO IMPROVE THEIR LIVES AND THE LIVES OF OTHERS.</p>
                              </div>
                         </article>
                    </li>
                    <li>
                         <article>
                              <header>
                                   <address>
                                        <a href="#">What we do with our money</a>
                                   </address>
                              </header>
                              <div class="comcont">
                                   <p>Making sure as much money as possible goes towards life-changing projects is incredibly important to us. In fact, it�s what we�re here for. For every cent received goes to a huge variety of projects, both big and small � from changing lives to empowering new class of entrepreneurs. </p>
                              </div>
                         </article>
                    </li>
               </ul>
          </div>
     </main>
</div>
@endsection