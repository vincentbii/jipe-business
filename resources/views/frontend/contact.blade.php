@extends('frontend.layouts.app')
@section('content')
@if (session('status'))
               <div class="alert alert-success">
                    {{ session('status') }}
               </div>
               @endif
<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <main class="hoc container clear">
          <div class="box box-success">   
               <div class="box-header with-border">
                    <h3 class="box-title">{{ 'Contact Us' }}</h3>
               </div>

               <div class="box-body">
                    <div class="table-responsive">


                         {{Form::open(['route'=>'frontend.contactus'])}}
                         
                         {{Form::label('name', 'Your Name')}}
                         {{Form::text('name', null, ['class'=>'form-control input-sm', 'placeholder' => 'Your Name', 'required' => 'required'])}}

                         {{Form::label('email', 'Your Email Address')}}
                         {{Form::email('email', null, ['class' => 'form-control input-sm', 'placeholder' => 'Your Email', 'required' => 'required'])}}

                         {{Form::label('message', 'Message')}}
                         {{Form::textarea('message', null, ['class'=>'form-control input-sm', 'required'=>'required', 'placeholder' => 'Your Message'])}}
                        
                        
                         <hr />
                         {{Form::submit('Submit', ['class' => 'btn btn-primary btn-sm'])}}


                         {{Form::close()}}

                    </div>
               </div>
          </div>


     </main>
</div>
@endsection
@section('after-scripts')
@if (config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
@endsection