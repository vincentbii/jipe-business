@extends('frontend.layouts.app')

@section('content')

<div class="bgded overlay" style="background-image:url('img/jipe3.jpg');">
     <div id="pageintro" class="hoc clear"> 
          <!-- ################################################################################################ -->
          <div class="flexslider basicslider">
               <ul class="slides">
                    <li>
                         <article>
                              <p>Jipe Business Limited</p>
                              <h3 class="heading">We are about creating financial freedom</h3>
                              <p></p>
                              <footer>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn btn-primary' ])}}</footer>
                         </article>
                    </li>
                    <li>
                         <article>
                              <p>Jipe Business Limited</p>
                              <h3 class="heading">We make dreams come through</h3>
                              <p></p>
                              <footer>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn btn-primary' ])}}</footer>
                         </article>
                    </li>
                    <li>
                         <article>
                              <p>Jipe Business Limited</p>
                              <h3 class="heading">Be your own boss</h3>
                              <p>You can become your own boss with only Ksh 100.</p>
                              <footer>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn btn-primary' ])}}</footer>
                         </article>
                    </li>
               </ul>
          </div>
          <!-- ################################################################################################ -->
     </div>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay light">
     <div id="pageintro" class="hoc clear"> 
          <!-- ################################################################################################ -->
          <article>
               <h2 class="heading">Want To Be A Success Story? Start Now</h2>
               <p>We are about creating financial freedom and making dreams come true. Join our fast-growing network of financially independent youths
                    and let your money work for you. you can become your own boss with only Ksh 100.</p>
               <footer>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn btn-primary' ])}}</footer>
          </article>
          <!-- ################################################################################################ -->
     </div>
</div>
<!--<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/02.png');">
  <article class="hoc container clear"> 
     ################################################################################################ 
    <div class="three_quarter first">
      <h6 class="nospace">Want To Be A Success Story? Start Now</h6>
      <p class="nospace">We are about creating financial freedom and making dreams come true. Join our fast-growing network of financially 
           independent youths and let your money work for you. you can become your own boss with only Ksh 100.</p>
    </div>
    <footer class="one_quarter">{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn' ])}}</footer>
     ################################################################################################ 
  </article>
</div>-->
<div class="wrapper bgded overlay" style="background-image:url('img/jipe8.jpg');">
  <article class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="three_quarter first">
      <p class="btmspace-50 justified">
               There are high level of unemployment in the country and the urge to become financially 
               independent and take charge of your own destiny is everyone's dream.
               It is becoming increasingly imperative to take the risks that may open
               up a new world for you. At JIPE BUSINESS ltd we have launched products that will
               sooner rather than later will make you be your own boss.
               JIPE BUSINESS is our latest product that will have young poeple start their journey,
               Thinking about starting your own business? JIPE business is here to help you get started
               to financial freedom.
          </p>
          <p>
               Are you a school leaver or College/University graduate desperately looking for a job?
               Do you want to become the next millionaire from your home area by starting your own business? 
               Our unique entrepreneurship support program seeks to grow entrepreneurs from as little as Ksh 100.
               You are only required to submit your business proposal where u are assured of funding (business grant) 
               from ksh 3,000 upto Ksh 1,000,000 .</p>
          {{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => 'btn btn-primary' ])}}
    </div>
    <footer class="one_quarter"><img src="{{asset('img/jipe1.png')}}" /></footer>
    <!-- ################################################################################################ -->
  </article>
</div>

<div class="wrapper bgded overlay light" style="background-image:url('img/jipe8.jpg');">
     <section class="hoc container clear"> 
          <!-- ################################################################################################ -->
          <div class="sectiontitle">
               <h3 class="heading">About Jipe Business</h3>
               <!--<p>Id eros nec finibus convallis nulla curabitur enim est egestas.</p>-->
          </div>
          <ul class="nospace group services">
               <li class="one_third first">
                    <article><a href="#"><i class="icon fa fa-eye"></i></a>
                         <h6 class="heading">Vision Statement</h6>
                         <p>JIPE BUSINESS envisions a society where all people have access to business opportunities, skills, resources  they need to actualize their full  potentials.</p>
                         <footer><a href="{{url('about')}}">Read More</a></footer>
                    </article>
               </li>
               <li class="one_third">
                    <article><a href="#"><i class="icon fa fa-bank"></i></a>
                         <h6 class="heading">Core Values</h6>
                         Respect and Honesty, Accountability and Integrity, Progress, and Kindness

                         <footer><a href="{{url('about')}}">Read More</a></footer>
                    </article>
               </li>
               <li class="one_third">
                    <article><a href="#"><i class="icon fa fa-cny"></i></a>
                         <h6 class="heading">Our core Purposes</h6>
                         <p>JIPE BUSINESS CORE PURPOSE IS TO EMPOWER AND GROW A NETWORK OF ENTREPRENUERS  TO IMPROVE THEIR LIVES AND THE LIVES OF OTHERS</p>
                         <footer><a href="{{url('about')}}">Read More</a></footer>
                    </article>
               </li>
          </ul>
          <!-- ################################################################################################ -->
          <div class="clear"></div>
     </section>
</div>
{!! Html::script('layout/scripts/jquery.min.js') !!}
{!! Html::script('layout/scripts/jquery.flexslider-min.js') !!}
{!! Html::script('layout/scripts/jquery.backtotop.js') !!}
{!! Html::script('layout/scripts/jquery.mobilemenu.js') !!}

@endsection