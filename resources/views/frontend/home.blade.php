<div class="content">
     <div class="title m-b-md">
          Welcome <br>to <br>Jipe Business Limited
     </div>
</div>

<div id="contents">
     <div class="highlight">
          <div class="clearfix">
               <div class="testimonial">
                    <h2>Testimonials</h2>
                    <p>
                         &ldquo;Aenean ullamcorper purus vitae nisl tristique sollicitudin. Quisque vestibulum, erat ornare.&rdquo;
                    </p>
                    <span>-John Doe and Jane Doe-</span>
               </div>
               <h1>Welcome to Jipe Business Limited</h1>
               <p>
                    We are about creating financial freedom and making dreams come true.
                    Join our fast-growing network of financially independent youths  and let your money work for you.
                    You can become your own boss with only Ksh 1,000. 
               </p>
               <h1>Want To Be A Success Story? Start Now</h1>
               <p>
                    There are high level of unemployment in the country and the urge to become financially independent and take charge of your own destiny is everyone's dream. It is becoming increasingly imperative to take the risks that may open up a new world for you. At JIPE BUSINESS ltd we have launched products that will sooner rather than later will make you be your own boss. JIPE BUSINESS is our latest product that will have young poeple start their journey,
               </p>
               <p>
                    Thinking about starting your own business? JIPE business is here to help you get started to financial freedom.

               </p>
          </div>
     </div>

     <div class="featured">
          <h2>Why Choose Us?</h2>
          <ul class="clearfix">
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/meeting.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our vision Statement</b> JIPE BUSINESS envisions a society where all people have access to business opportunities, skills, resources  they need to actualize their full  potentials.
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>
               </li>
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/meeting.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our Core Values</b> Respect and Honesty, Accountability and Integrity, Progress and Kindness
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>
               </li>
               <li>
                    <div class="frame1">
                         <div class="box">
                              <img src="images/smile.jpg" alt="Img" height="130" width="197">
                         </div>
                    </div>
                    <p>
                         <b>Our Core Purpose</b> JIPE BUSINESS CORE PURPOSE IS TO EMPOWER AND GROW A NETWORK OF ENTREPRENUERS  TO IMPROVE THEIR LIVES AND THE LIVES OF OTHERS.
                    </p>
                    <a href="{{url('about')}}" class="btn btn-info btn-sm">Read More</a>

               </li>
          </ul>

     </div>
</div>


<!--@extends('frontend.layouts.app')-->
<!--
@section('content')
<div class="row">

     @role('Administrator')
     {{-- You can also send through the Role ID --}}

     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.role') . trans('strings.frontend.tests.using_blade_extensions') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 1: ' . trans('strings.frontend.tests.you_can_see_because', ['role' => trans('roles.administrator')]) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endauth

     @if (access()->hasRole('Administrator'))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.role') . trans('strings.frontend.tests.using_access_helper.role_name') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 2: ' . trans('strings.frontend.tests.you_can_see_because', ['role' => trans('roles.administrator')]) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     @if (access()->hasRole(1))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.role') . trans('strings.frontend.tests.using_access_helper.role_id') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 3: ' . trans('strings.frontend.tests.you_can_see_because', ['role' => trans('roles.administrator')]) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     @if (access()->hasRoles(['Administrator', 1]))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.role') . trans('strings.frontend.tests.using_access_helper.array_roles_not') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 4: ' . trans('strings.frontend.tests.you_can_see_because', ['role' => trans('roles.administrator')]) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     {{-- The second parameter says the user must have all the roles specified. Administrator does not have the role with an id of 2, so this will not show. --}}
     @if (access()->hasRoles(['Administrator', 2], true))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.role') . trans('strings.frontend.tests.using_access_helper.array_roles') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.tests.you_can_see_because', ['role' => trans('roles.administrator')]) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     @permission('view-backend')
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.permission') . trans('strings.frontend.tests.using_access_helper.permission_name') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 5: ' . trans('strings.frontend.tests.you_can_see_because_permission', ['permission' => 'view-backend']) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endauth

     @if (access()->hasPermission(1))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.permission') . trans('strings.frontend.tests.using_access_helper.permission_id') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 6: ' . trans('strings.frontend.tests.you_can_see_because_permission', ['permission' => 'view_backend']) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     @if (access()->hasPermissions(['view-backend', 1]))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.permission') . trans('strings.frontend.tests.using_access_helper.array_permissions_not') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.test') . ' 7: ' . trans('strings.frontend.tests.you_can_see_because_permission', ['permission' => 'view_backend']) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif

     @if (access()->hasPermissions(['view-backend', 2], true))
     <div class="col-xs-12">

          <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-home"></i> {{ trans('strings.frontend.tests.based_on.permission') . trans('strings.frontend.tests.using_access_helper.array_permissions') }}</div>

               <div class="panel-body">
                    {{ trans('strings.frontend.tests.you_can_see_because_permission', ['permission' => 'view_backend']) }}
               </div>
          </div> panel 

     </div> col-md-10 
     @endif
</div>row
@endsection-->