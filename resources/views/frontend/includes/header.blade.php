<!DOCTYPE HTML>
<html>
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="csrf-token" content="{{ csrf_token() }}">

          <title>@yield('title', app_name())</title>

          <!-- Meta -->
          <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
          <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
          @yield('meta')

          <!-- Styles -->
          @yield('before-styles')

          <!-- Check if the language is set to RTL, so apply the RTL layouts -->
          <!-- Otherwise apply the normal LTR layouts -->
          @langRTL
          {{ Html::style(getRtlCss('css/frontend.css')) }}
          @else
          {{ Html::style('css/frontend.css') }}
          @endif

          @yield('after-styles')
          
          

          {{ Html::style('css/style.css') }}

          <!-- Scripts -->
          <script>
               window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
          </script>
          
     </head>
     <body id="app-layout">
          <div id="app">
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.menu')

            <div class="container">
                @include('includes.partials.messages')
                
            </div><!-- container -->
        </div><!--#app-->
          
          @yield('content')
     </body>
</html>