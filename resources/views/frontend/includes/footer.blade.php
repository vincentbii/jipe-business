<div class="wrapper row4">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_third first">
      <h6 class="heading">Contact Us</h6>
      <ul class="nospace btmspace-30 linklist contact">
<!--        <li><i class="fa fa-map-marker"></i>
          <address>
          Street Name &amp; Number, Town, Postcode/Zip
          </address>
        </li>-->
        <li><i class="fa fa-phone"></i> +254(797) 648 693</li>
        <li><i class="fa fa-envelope-o"></i> info@jipebusiness.coke</li>
      </ul>
      <ul class="faico clear">
        <li><a class="faicon-facebook" target="_blank" href="https://web.facebook.com/www.jipebusiness.co.ke/"><i class="fa fa-facebook"></i></a></li>
        <!--<li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>-->
        <!--<li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>-->
        <!--<li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>-->
        <!--<li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>-->
        
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Quick Links</h6>
      <ul class="nospace linklist">
        <li>
          <article>
            <h2 class="nospace font-x1"><a href="{{url('/grantprogram')}}">Grant Program</a></h2>
            <h2 class="nospace font-x1"><a href="{{url('/about')}}">About Us</a></h2>
           </article>
        </li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Core Values</h6>
      <p class="nospace btmspace-30">Respect and Honesty, Accountability and Integrity, Progress, and Kindness</p>
      
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy; 2017 - All Rights Reserved - <a href="{{url('/')}}">Jipe Business Limited</a></p>
    <p class="fl_right">Designed by <a target="_blank" href="http://asterisktech.co.ke/" title="Free Website Templates">Asterisk Technologies</a></p>
    <!-- ################################################################################################ -->
  </div>
</div>