<div class="wrapper row1">
     <header id="header" class="hoc clear"> 
          <!-- ################################################################################################ -->
          <div id="" class="fl_left">
               <image img src="{{url('img/logo_jipe.png')}}" width="100px" heigth="100px" alt="Logo"></image>
          </div>
          <div class="fl_right">
               <ul class="nospace">
                    <li style="float: left"><i class="fa fa-phone"></i> +254(797) 648 693 &nbsp;&nbsp;</li>
                    <li style="float: left"><i class="fa fa-envelope-o"></i> info@jipebusiness.co.ke</li>
               </ul>
          </div>

     </header>
</div>
<div class="wrapper row0">
     <div id="topbar" class="hoc clear"> 
          <!-- ################################################################################################ -->
          
          <div class="fl_right">
               <ul class="nospace">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => active_class(Active::checkRoute('frontend.grantprogram')) ])}}</li>
                    <li>{{ link_to_route('frontend.about', 'About', ['class' => 'nospace']) }}</li>
                    <li><a href="{{'/contact'}}">Contact</a></li>
                    <li><a href="{{'/resellers'}}">Resellers</a></li>
                    
                    
                    @if ($logged_in_user)
                    <li><a href="{{ '/dashboard' }}">Dashboard</a></li>
                    @endif
                    @if (! $logged_in_user)
                    <li><a href="{{ 'login' }}">Login</a></li>
                    @if (config('access.users.registration'))
                    <li><a href="{{ 'register' }}">Register</a></li>
                    @endif
                    @else
                    <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                              {{ $logged_in_user->name }} <span class="caret"></span>
                         </a>
                         <ul class="dropdown-menu" role="menu">
                              @permission('view-backend')
                              <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                              @endauth
                              <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                              <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                         </ul>
                    </li>
                    @endif
               </ul>
          </div>
          <!-- ################################################################################################ -->
     </div>
</div>

