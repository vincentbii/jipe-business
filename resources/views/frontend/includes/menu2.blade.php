
<div style="background-color: #ffffff">
     <header id="header" class="hoc clear">
                              <div id="logo" class="fl_left">
                                   <a href="index.html"><img src={{asset('img/logo1.png')}} width="100px" height="100px" alt="Jipe Business Limited"></a>
                              </div>
          <nav id="mainav" class="fl_right">
               <ul class="clear">
                    <li >{{link_to_route('frontend.index', 'Home', [], ['class' => active_class(Active::checkRoute('frontend.')) ])}}</li>
                    <li>{{link_to_route('frontend.about', 'About Us', [], ['class' => active_class(Active::checkRoute('frontend.about')) ])}}</li>
                    <li>{{link_to_route('frontend.grantprogram', 'JB Grant Program', [], ['class' => active_class(Active::checkRoute('frontend.grantprogram')) ])}}</li>
                    <li>{{link_to_route('frontend.contact', 'Contact Us', [], ['class' => active_class(Active::checkRoute('frontend.contact')) ])}}</li>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    @if ($logged_in_user)
                    <li>{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard'), [], ['class' => active_class(Active::checkRoute('frontend.user.dashboard')) ]) }}</li>
                    @endif
                    @if (! $logged_in_user)
                    <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login'), [], ['class' => active_class(Active::checkRoute('frontend.auth.login')) ]) }}</li>
                    @if (config('access.users.registration'))
                    <li>{{ link_to_route('frontend.auth.register', trans('navs.frontend.register'), [], ['class' => active_class(Active::checkRoute('frontend.auth.register')) ]) }}</li>
                    @endif
                    @else
                    <li class="">
                         <a href="#" class="drop" data-toggle="" role="button" aria-expanded="false">
                              {{ $logged_in_user->name }}
                         </a>

                         <ul class="dropdown" role="menu">
                              @permission('view-backend')
                              <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                              @endauth
                              <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                              <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                         </ul>
                    </li>
                    @endif
                    <li><a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a></li>
               </ul>
          </nav>
     </header>
</div>
<div style="height: 5px;
     background: #00aeef;">

</div>
<script>
     function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }

</script>