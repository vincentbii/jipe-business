<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller {
     public function about(){
          return view('frontend.about');
     }

     public function addItem(Request $request) {
          $rules = array(
              'name' => 'required'
          );
          $validator = Validator::make(Input::all(), $rules);
          if ($validator->fails())
               return Response::json(array(
                           'errors' => $validator->getMessageBag()->toArray()
                       ));
          else {
               $data = new Data ();
               $data->name = $request->name;
               $data->save();
               return response()->json($data);
          }
     }

}
