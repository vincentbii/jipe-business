<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Models\Country;
use App\Models\Subcounty;
use App\Models\County;
//use App\Models\CountryOfInvestment;
//use App\Models\BusinessSector;
use App\Models\Education;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller {

//     public function showregistrationform() {
//          
//     }

     use RegistersUsers;

     /**
      * @var UserRepository
      */
     protected $user;

     /**
      * RegisterController constructor.
      *
      * @param UserRepository $user
      */
     public function __construct(UserRepository $user) {
          // Where to redirect users after registering
          $this->redirectTo = route(homeRoute());

          $this->user = $user;
     }

     /**
      * Show the application registration form.
      *
      * @return \Illuminate\Http\Response
      */
     public function showRegistrationForm() {
          $education_level = Education::all();
          $county = County::all();
          $country = Country::all();
//          $country = \DB::table('country')->lists('id', 'name');
          $sub_county = Subcounty::all();
//          $payment_method = \DB::table('payment_method')->pluck('name', 'id')->toArray();
          $payment_method = \DB::table('payment_method')->get();

          $affcode = null;
          return view('frontend.auth.register', [
              'country' => $country, 'county' => $county, 'education_level' => $education_level, 'sub_county' => $sub_county, 'payment_method' => $payment_method, 'affcode' => $affcode
          ]);
//          return view('frontend.auth.register');
     }

     public function showRegistrationFormAff($affcode) {
          $education_level = Education::all();
          $county = County::all();
          $country = Country::all();
//          $country = \DB::table('country')->lists('id', 'name');
          $sub_county = Subcounty::all();
//          $payment_method = \DB::table('payment_method')->pluck('name', 'id')->toArray();
          $payment_method = \DB::table('payment_method')->get();
          return view('frontend.auth.register', [
              'country' => $country, 'county' => $county, 'education_level' => $education_level, 'sub_county' => $sub_county, 'payment_method' => $payment_method, 'affcode' => $affcode
          ]);
//          return view('frontend.auth.register');
     }

     /**
      * @param RegisterRequest $request
      *
      * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
      */
     public function register(RegisterRequest $request) {
          if (config('access.users.confirm_email')) {
               $user = $this->user->create($request->only('id_number', 'affcode', 'education_level', 'county', 'first_name', 'last_name', 'email', 'password', 'number', 'country', 'payment_method'));
               event(new UserRegistered($user));
               return redirect($this->redirectPath());
          } else {
               access()->login($this->user->create($request->only('id_number', 'affcode', 'county', 'education_level', 'first_name', 'last_name', 'email', 'password', 'number', 'country', 'payment_method')));
               event(new UserRegistered(access()->user()));

               return redirect($this->redirectPath());
          }
     }

}
