<?php

namespace App\Http\Controllers\Frontend\User;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Comments;

class CommentsController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index($id) {
          $comments = \DB::table('comments')
                  ->where('post_id', $id)
                  ->get();
          return view('frontend.user.comments.index', ['comments' => $comments]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {

          return view('frontend.user.comments.create', compact('id'));
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
          $user = Auth::user();
          $comment = new Comments;
          $comment['user_id'] = $user->id;
//          $comment['post_id'] = $request->get('plan_id');
          $comment['post_id'] = (int)$request->get('plan_id');
          $comment['comment'] = $request->get('comment');
          $comment['commentor_id'] = $user->id;
          
          $proposal = \DB::table('business_plan')
                  ->where('business_plan.id', (int)$request->get('plan_id'))
                  ->join('users', 'users.id', '=', 'business_plan.user_id')
                  ->join('comments', 'comments.post_id', '=', 'business_plan.id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'users.id_number', 'business_plan.*', 'comments.comment')
                  ->first();
          
          if($comment->save()){
               Mail::send('comment_user', ['data' => $proposal, 'user' => $user], function($message) {
                    $user = Auth::user();
                    $message->from($user->email, $user->first_name.' '.$user->last_name);
                    $message->to('info@jipebusiness.co.ke', 'Jipe Business Limited')->subject('Jipe Business Limited Notification!');
               });
          }
          return \Redirect::route('frontend.user.upload', array($comment->id))->with('message', 'Product added!');
          
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('comments')->where('id','=', $id)->delete();
          return back()->with('status', 'Comment deleted successfully');
//          return $id;
     }

}
