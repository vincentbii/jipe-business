<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Support\Facades\Auth;
use Pesapal;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller {

     public function makePayment() {
          $user = Auth::user();
          $amountPayable = \DB::table('amount_to_be_payed')
                  ->where('status', TRUE)
                  ->first();
          
          $amountPaid = \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['status2', 1],
                      ['post_id', NULL],
                      ['status', 'COMPLETED']
                  ])
                  ->sum('amount');
          $amount = ($amountPayable->amount - $amountPaid);
          
          return view('frontend.user.makepayment', ['user' => $user, 'amount' => $amount]);
     }

     public function payment(Request $request) {//initiates payment
          $user = \DB::table('users')
                  ->where('id_number', $request->get('id_number'))
                  ->first();
//          $user = Auth::user();
          \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['post_id', NULL],
                      ['status', 'PENDING']
                  ])
                  ->update([
                      'status2' => FALSE
          ]);

          $payments = new Payment;
          $payments->businessid = $user->id_number; //Business ID
          $payments->transactionid = Pesapal::random_reference();
          $payments->status = 'NEW'; //if user gets to iframe then exits, i prefer to have that as a new/lost transaction, not pending
          $payments->amount = $request->get('amount');
          $payments->user_id = $user->id;
          $payments->save();

          $details = array(
              'amount' => $payments->amount,
              'description' => 'Test Transaction',
              'type' => 'MERCHANT',
              'first_name' => $user->first_name,
              'last_name' => $user->last_name,
              'email' => $user->email,
              'phonenumber' => $user->number,
              'reference' => $payments->transactionid,
              'height' => '400px',
              'currency' => 'KES'
          );
          $iframe = Pesapal::makePayment($details);

          return view('frontend.user.pesapal.index', compact('iframe'));
     }

     public function paymentsuccess(Request $request) {//just tells u payment has gone thru..but not confirmed
          $user = Auth::user();


          $trackingid = $request->input('tracking_id');
          $ref = $request->input('merchant_reference');


          $payments = \DB::table('payment')
                  ->where('transactionid', '=', $ref)
                  ->update([
              'trackingid' => $trackingid,
              'status' => 'PENDING'
          ]);

          if ($trackingid !== NULL && $ref !== NULL) {
               $this->checkpaymentstatus($trackingid, $ref);
          }

//          $count = \DB::table('payment')
//                  ->where([
//                      ['user_id', '=', $user->id],
//                      ['status2', TRUE]
//                  ])
//                  ->count();

          $amountPayable = \DB::table('amount_to_be_payed')
                  ->where('status', TRUE)
                  ->first();

          $amountPaid = \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['status2', 1],
                      ['post_id', NULL],
                      ['status', 'COMPLETED']
                  ])
                  ->sum('amount');

          $proposals = \DB::table('business_plan')
                  ->where([
                      ['user_id', $user->id],
                      ['status', TRUE]
                  ])
                  ->get();

                  if ($ref !== NULL) {
               $payment = \DB::table('payment')
                  ->where('transactionid', '=', $ref)
                  ->first();
          }else{
                           $payment = \DB::table('payment')
                  ->where('user_id', '=', $user->id)
                  ->orderBy('date_created', 'DESC')
                  ->limit(1)
                  ->first();
          }

        

          $comments = \DB::table('comments')
                          ->where([
                              ['user_id', '=', $user->id],
                              ['status', '=', '1']
                          ])->count();


          return view('frontend.user.dashboard', [
                      'amountPaid' => $amountPaid,
                      'amountPayable' => $amountPayable,
                      'proposals' => $proposals,
                      'comments' => $comments,
                      'payment' => $payment,
                      'user' => $user
                  ]);
     }

     //This method just tells u that there is a change in pesapal for your transaction..
     //u need to now query status..retrieve the change...CANCELLED? CONFIRMED?
     public function paymentconfirmation(Request $request) {
      $user = Auth::user();
      $payment = \DB::table('payment')
      ->where('user_id', $user->id)
      ->orderBy('date_created', 'DESC')
      ->limit(1)
      ->first();

          $trackingid = $payment->trackingid;
          $merchant_reference = $payment->transactionid;
//          $pesapal_notification_type = $request->input('pesapal_notification_type');
          //use the above to retrieve payment status now..
          $this->checkpaymentstatus($trackingid, $merchant_reference);

          $transaction = \DB::table('payment')
                  ->where('trackingid', $request->input('trackingid'))
                  ->first();

          if ($transaction->status == 'COMPLETED') {
               return redirect('dashboard')->with('status', 'Your transaction was completed');
          } elseif ($transaction->status == 'PENDING') {
               return redirect('dashboard')->with('status', 'Your transaction is still PENDING! Confirm your transaction details');
          } elseif ($transaction->status == 'FAILED') {
               \DB::table('payment')
                       ->where('trackingid', $request->input('trackingid'))
                       ->update([
                           'status2' => FALSE
               ]);
               return redirect('dashboard')->with('status', 'Your transaction FAILED! Make another transaction');
          }elseif ($transaction->status == 'INVALID') {
            return redirect('dashboard');
          }
     }

     //Confirm status of transaction and update the DB
     public function checkpaymentstatus($trackingid, $ref) {
          $status = Pesapal::getMerchantStatus($ref);
          $payments = Payment::where('trackingid', $trackingid)->first();
          $payments->status = $status;
//          $payments->payment_method = '';; //use the actual method though...
          $payments->save();
          return "success";
     }

}
