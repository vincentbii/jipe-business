<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ResellerController extends Controller
{

    public function reseller(){
    	$affiliate = Auth::user();
    	if($affiliate->affiliate == 1){
            $aff_signups = \DB::table('user_invitations')
                ->where('user_id', Auth::user()->id)
                ->where('status', 'successful')
                ->count();

            $aff_clicks = \DB::table('user_invitations')
                ->where('user_id', Auth::user()->id)
                ->count();
            
            $aff_conversion = '';

            if($aff_clicks !== 0){
                $aff_conversion = (($aff_signups / $aff_clicks) * 100);
                $aff_conversion = round($aff_conversion, '1');
            }else{
                $aff_conversion = 0;
            }

    		$url = url('/affiliate');
    		$refCode = \Invite::invite('biivincent@gmail.com', Auth::user()->id);
    		$affCode = $url.'/'.$refCode;

            $commission = \DB::table('affCommission')
                ->first();

            $commissionOne = \DB::table('commission')
                ->where('user_id', Auth::user()->id)
                ->first();

            $commissionTwo = ($aff_signups * $commission->commission);

            if($commissionOne == null){
                $total_commission = $commissionTwo;
            }else{
                $total_commission = ($commissionOne->total_commission + $commissionTwo);
            }

            $comm_request = \DB::table('comm_with_request')
                ->where('user_id', Auth::user()->id)
                ->where('status', true)
                ->first();

    		return view('frontend.user.affiliation.affiliate',[
                'affCode' => $affCode, 
                'aff_clicks' => $aff_clicks, 
                'aff_signups' => $aff_signups, 
                'aff_conversion' => $aff_conversion,
                'total_commission' => $total_commission,
                'commissionOne' => $commissionOne,
                'comm_request' => $comm_request
            ]);
                return $total_commission;
    	}else{
    		return view('frontend.user.affiliation.notaffiliate');
    	}
    }

    public function activateReseller(){
    	$reseller = Auth::user();
    	$reseller->affiliate = true;
    	$reseller->save();

    	return redirect('resellers');
    }

    public function requestWith(Request $request){
        $this->closeActiveInvitations(Auth::user()->id);

        \DB::table('comm_with_request')
            ->insert([
                'user_id'=> Auth::user()->id
            ]);

            return back()->withInput(['status', 'You have requested for commission withdrawal']);

    }

    public function closeActiveInvitations($user_id){
        $invitations = \DB::table('user_invitations')
            ->where('user_id', $user_id)
            ->where('status', 'successful')
            ->count();

        $commission = \DB::table('affCommission')
                ->first();

        $total_commission = $invitations * $commission->commission;

        $insertCommision = \DB::table('commission')
            ->insert([
                'user_id' => $user_id,
                'total_commission' => $total_commission
            ]);

        if($insertCommision){
            \DB::table('user_invitations')
                ->where('user_id', $user_id)
                ->where('status', 'successful')
                ->update([
                    'status' => 'COMPLETED'
                ]);
        }

        return $total_commission;
    }

    
}
