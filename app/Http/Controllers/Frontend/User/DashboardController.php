<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
      return redirect('paymentsuccess');
         /*
         $user = Auth::user();
         
         $comments = \DB::table('comments')
                 ->where([
                     ['user_id' , '=', $user->id],
                     ['status' , '=', '1']
                 ])->count();
         
         $proposals = \DB::table('business_plan')
                  ->where([
                      ['user_id', '=', $user->id],
                      ['status', TRUE]
                          ])
                  ->get();
         
         $amountPayable = \DB::table('amount_to_be_payed')
                  ->where('status', TRUE)
                  ->first();

          $amountPaid = \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['status2', 1],
                      ['post_id', NULL],
                      ['status', 'COMPLETED']
                  ])
                  ->sum('amount');
         
        return view('frontend.user.dashboard', [
            'amountPaid' => $amountPaid,
            'amountPayable' => $amountPayable,
            'proposals' => $proposals,
            'comments' => $comments
                ]);

                */
    }
}
