<?php

namespace App\Http\Controllers\Frontend\User;

use Mail;
use App\Models\BusinessPlan;
//use App\Models\CountryOfInvestment;
use App\Models\BusinessSector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class BusinessPlanController extends Controller {

     public function businessProposals() {
          $user = Auth::user();

          $proposals = \DB::table('business_plan')
                  ->where([
                      ['business_plan.user_id', '=', $user->id],
                      ['business_plan.status', 1]
                  ])
                  ->join('business_sector', 'business_sector.id', '=', 'business_plan.business_sectors')
                  ->join('country', 'country.id', '=', 'business_plan.country_of_investment')
                  ->select('business_plan.user_id', 'business_plan.id', 'business_plan.doc_name', 'business_plan.doc_directory', 'business_plan.amount_to_be_financed', 'business_plan.description', 'business_sector.name', 'country.country_name')
                  ->get();


          $comments = \DB::table('comments')
                  ->orderBy('date_created', 'ASC')
                  ->join('users', 'users.id', '=', 'comments.commentor_id')
                  ->select('comments.*', 'users.first_name', 'users.last_name')
                  ->get();




          return view('frontend.user.businessproposals', ['proposals' => $proposals, 'comments' => $comments]);
     }
     
     public function preview($id){
          $file = \DB::table('business_plan')
                  ->where('id' ,$id)
                  ->first();
          
          return view('frontend.user.preview', ['file'=>$file]);
     }
     public function download($id) {
          $file = \DB::table('business_plan')
                  ->where('id', $id)
                  ->first();


          return response()->file($file->doc_directory . '/' . $file->doc_name);
     }

     public function uploadproposal() {
          $country_of_investment = \DB::table('countryofinvestment')
                  ->join('country', 'country.id', '=', 'countryofinvestment.country_id')
                  ->select('country.country_name', 'countryofinvestment.country_id', 'countryofinvestment.id')
                  ->get();
          $business_sectors = BusinessSector::pluck('name', 'id')->toArray();
          return view('frontend.user.uploadproposal', [
              'business_sectors' => $business_sectors,
              'country_of_investment' => $country_of_investment
          ]);
     }

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $country_of_investment = \DB::table('countryofinvestment')
                  ->join('country', 'country.id', '=', 'countryofinvestment.country_id')
                  ->select('country.country_name', 'countryofinvestment.country_id', 'countryofinvestment.id')
                  ->get();
          $business_sectors = BusinessSector::pluck('name', 'id')->toArray();
          $user = Auth::user();

          $payment = \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['status2', TRUE],
                      ['post_id', NULL]
                  ])
                  ->orderBy('date_created', 'desc')
                  ->first();

          $proposals = \DB::table('business_plan')
                  ->where([
                      ['business_plan.user_id', '=', $user->id],
                      ['business_plan.status', 1]
                  ])
                  ->join('business_sector', 'business_sector.id', '=', 'business_plan.business_sectors')
                  ->join('country', 'country.id', '=', 'business_plan.country_of_investment')
                  ->select('business_plan.user_id', 'business_plan.id', 'business_plan.doc_name', 'business_plan.doc_directory', 'business_plan.amount_to_be_financed', 'business_plan.description', 'business_sector.name', 'country.country_name')
                  ->get();

          $proposal = \DB::table('business_plan')
                  ->where([
                      ['user_id', '=', $user->id],
                      ['status', TRUE]
                  ])
                  ->first();
          $comments = \DB::table('comments')
                  ->orderBy('date_created', 'ASC')
                  ->join('users', 'users.id', '=', 'comments.commentor_id')
                  ->select('comments.*', 'users.first_name', 'users.last_name')
                  ->get();

          return view('frontend.user.upload', [
              'proposals' => $proposals,
              'comments' => $comments,
              'business_sectors' => $business_sectors,
              'country_of_investment' => $country_of_investment,
              'proposal' => $proposal,
              'payment' => $payment
          ]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
          $user = Auth::user();
          $filename = '';
          if(!$request->file()){
               $filename = uniqid() . $user->first_name;
               $filepath = NULL;
          }  else {
               $filename = uniqid() . $request->file->getClientOriginalName();
               $request->file->move(public_path('files') . '/business_plans', $filename);
               
               $filepath = public_path('files/business_plans');
          }
          
          
//
//          if ($request->get('file') == NULL) {
//                $filename = uniqid();
//               $filepath = NULL;
//          }
//          
//          if($request->get('file') !== NULL) {
//               $filename = uniqid() . $request->file->getClientOriginalName();
//               $request->file->move(public_path('files') . '/business_plans', $filename);
//               $filepath = public_path('files/business_plans');
//              
//          }
//
//
//
////          $uniqueFileName = uniqid() .''. $request->get('upload_file')->getClientOriginalName() . '' . $request->get('upload_file')->getClientOriginalExtension());
////          $request->$request->file->move(base_path().public_path('files') . $filename);
////          $file_name = $user->id . '-' . $user->first_name . '-' . $request->file->getClientOriginalName();
//
          $status = \DB::table('business_plan')->insert([
              'user_id' => $user->id,
              'doc_name' => $filename,
              'amount_to_be_financed' => $request->get('amount_to_be_financed'),
              'business_sectors' => $request->get('business_sector'),
              'country_of_investment' => $request->get('country_id'),
              'description' => $request->get('description'),
              'doc_directory' => $filepath
          ]);

          $post = \DB::table('business_plan')
                  ->where([
                      ['user_id', $user->id],
                      ['status', true]
                  ])
                  ->first();

          $post_id = $post->id;
          $user_id = $post->user_id;

          $this->assignPayment($post_id, $user_id);

          if ($status) {
               Mail::send('index', ['key' => 'value'], function($message) {
                    $user = Auth::user();
                    $message->from('biivincent4@gmail.com', 'Jipe Business Limited');
                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Jipe Business Limited Notification!');
               });
          }

          return \Redirect::route('frontend.user.upload')->with('message', 'Successfully Uploaded!');
     }

     public function assignPayment($post_id) {
          $user = Auth::user();
          $amountPayable = \DB::table('amount_to_be_payed')
                  ->where('status', TRUE)
                  ->first();

          $amountPaid = \DB::table('payment')
                  ->where([
                      ['user_id', $user->id],
                      ['status2', 1],
                      ['post_id', NULL],
                      ['status', 'COMPLETED']
                  ])
                  ->sum('amount');

          if ($amountPaid >= $amountPayable->amount) {
               $extraAmount = $amountPaid - $amountPayable->amount;
               \DB::table('payment')
                       ->where([
                           ['user_id', $user->id],
                           ['status2', 1],
                           ['post_id', NULL],
                           ['status', 'COMPLETED']
                       ])
                       ->update(['post_id' => $post_id]);
               if ($extraAmount > 0) {
                    \DB::table('payment')
                            ->insert([
                                'user_id' => $user->id,
                                'status' => 'COMPLETED',
                                'amount' => $extraAmount,
                                'businessid' => $user->id_number
                    ]);
               }
          }

          return "success";
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
       \*
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request) {
          $update = \DB::table('business_plan')
                  ->where('id', $request->get('id'))
                  ->update([
                      'description' => $request->get('description')
          ]);
          
          if($update){
               return \Redirect::route('frontend.user.upload')->with('status', 'File Updated Successfully');
          }  else {
               return \Redirect::route('frontend.user.upload')->with('status', 'No changes were made to your proposal');
          }
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          $doc = BusinessPlan::findOrFail($id);

////          $doc = BusinessPlan::where('id', '=', $id);
//          $directory = $doc->doc_directory;
//          $dir = $directory.''.$doc->doc_name;
//          Storage::delete($dir);
//          return $dir;
//          $doc_dir = \DB::table('business_plan')->where('id', $id);
          $file = $doc->doc_name;
//          $file = $doc_dir->your_file_path;
          $filename = base_path() . '/storage/business_plans/' . $file;
          \File::delete($filename);
          \DB::table('business_plan')->where('id', '=', $id)->delete();

          return \Redirect::route('frontend.user.upload', array($doc->id))->with('message', 'File deleted Successfully');
     }

}
