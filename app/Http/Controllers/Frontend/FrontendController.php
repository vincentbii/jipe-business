<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
     public function contactus(Request $request){
     
    
          \DB::table('messages')
                  ->insert([
                      'name'=>$request->get('name'),
                      'email'=> $request->get('email'),
                      'message' => $request->get('message')
                  ]);
          
          $data = \DB::table('messages')
                  ->where('email', $request->get('email'))
                  ->first();
      
          $mail = Mail::send('contactus', ['data' => $data], function($message) {
                       $message->to('info@jipebusiness.co.ke')->subject('Notifications - Jipe Contact Form');
                  });
                  
          $mail = Mail::send('contactus', ['data' => $data], function($message) use ($data) {
                       $message->to($data->email)->subject('Email Sent');
                  });

               
               return redirect('/contact')->with('status', 'Your form has been successfully submitted');
          
     }

     public function registertrial(){
          return view('frontend.registertest');
     }

     public function contact(){
          return view('frontend.contact');
     }

     public function about(){
          return view('frontend.about');
     }
     
     public function grant(){
          return view('frontend.grant');
     }

     /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }
}
