<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
//use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Models\BusinessPlan;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\File;
use Mail;
use Illuminate\Support\Facades\Auth;

class BusinessPlanController extends Controller {

     public function download($id) {
          $file = \DB::table('business_plan')
                  ->where('id', $id)
                  ->first();

//          $file = $file->doc_directory . '/' . $file->doc_name;

          return response()->file($file->doc_directory . '/' . $file->doc_name);
     }
     
     public function preview($id){
          $file = \DB::table('business_plan')
                  ->where('id' ,$id)
                  ->first();
          
          return view('backend.uploads.preview', ['file'=>$file]);
     }

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $users = \DB::table('users')
                  ->where('business_plan.status', '=', TRUE)
                  ->join('business_plan', 'users.id', '=', 'business_plan.user_id')
                  ->select('users.first_name', 'users.id_number', 'users.email', 'users.last_name', 'business_plan.*')
                  ->get();


//          $business_plan = \DB::table('business_plan')
////                 ->where('preview', '=', FALSE)
//                  ->join('users', 'users.id', '=', 'business_plan.user_id')
//                  ->get();

          return view('backend.uploads.index', ['business_plan' => $users]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
//
     }

     public function deactivated() {

          $deactivated = \DB::table('business_plan')
                  ->where(
                          'business_plan.status', '=', FALSE
                  )
                  ->join('users', 'users.id', '=', 'business_plan.user_id')
                  ->select('users.first_name', 'users.id_number', 'users.email', 'users.last_name', 'business_plan.*')
                  ->get();


          return view('backend.uploads.deactivated', ['deactivated' => $deactivated]);
     }

     public function approved() {
          $approved = \DB::table('business_plan')
                  ->where(
                          'approved', '=', TRUE
                  )
                  ->join('users', 'users.id', '=', 'business_plan.user_id')
                  ->select('users.first_name', 'users.id_number', 'users.email', 'users.last_name', 'business_plan.*')
                  ->get();


          return view('backend.uploads.approved', ['approved' => $approved]);
     }

     public function financed() {

          $financed = \DB::table('business_plan')
                  ->where(
                          'business_plan.status', '=', TRUE
                  )
                  ->join('users', 'users.id', '=', 'business_plan.user_id')
                  ->select('users.first_name', 'users.id_number', 'users.email', 'users.last_name', 'business_plan.*')
                  ->get();


          return view('backend.uploads.financed', ['financed' => $financed]);
     }

     public function unfinance($id) {
          \DB::table('financed')
                  ->where('business_plan_id', $id)
                  ->update(['status' => FALSE]);

          \DB::table('business_plan')
                  ->where('id', $id)
                  ->update(['financed' => FALSE]);

          return back()->with('status', 'This project will no longer be financed');
     }

     public function deleted() {
          
     }

     public function disapprove($id) {

          \DB::table('business_plan')
                  ->where('id', $id)
                  ->update(['approved' => FALSE]);

          return back()->with('status', 'You have Disapproved this project');
     }

     public function approve($id) {
          \DB::table('business_plan')
                  ->where('id', $id)
                  ->update(['approved' => TRUE]);

          return back()->with('status', 'You have Approved this project');
     }

     public function deactivate($id) {
          \DB::table('business_plan')
                  ->where('id', $id)
                  ->update(['status' => 0]);

          $this->deactivatePayment($id);

          return back()->with('status', 'You have Deactivated this Business plan');
     }

     public function deactivatePayment($id) {
          \DB::table('payment')
                  ->where('post_id', $id)
                  ->update(['status2' => FALSE]);

          \DB::table('comments')
                  ->where('post_id', $id)
                  ->update(['status' => FALSE]);
     }

     public function activate($id) {
          \DB::table('business_plan')
                  ->where('id', $id)
                  ->update(['status' => TRUE]);

          return back()->with('status', 'This project is now active');
     }
     
     public function view($id){
          $details = \DB::table('business_plan')
                  ->where('id', $id)
                  ->first();
          
          return view('backend.uploads.view', ['details' => $details]);
     }

     public function finance(Request $request) {
          $finance = \DB::table('business_plan')
                  ->where('id', $request->get('id'))
                  ->update([
                    'financed' => TRUE,
                    'financed_amount' => $request->get('amount'),
                      'financed_note' => $request->get('note')
                  ]);
          $user = \DB::table('business_plan')
                  ->where('business_plan.id', $request->get('id'))
                  ->join('users', 'users.id', '=', 'business_plan.user_id')
                  ->join('financed', 'business_plan.id', '=', 'financed.business_plan_id')
                  ->select('users.email', 'users.first_name', 'users.last_name', 'financed.amount', 'financed.note')
                  ->first();

               return back()->with('status', 'You have agreed to finance this project');
          

          
     }



     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
//
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {


//
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $business_plan = BusinessPlan::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.uploads.edit', compact('business_plan'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          $commentor = Auth::user();
          $business_plan = BusinessPlan::find($id);
          $business_plan->preview = 1;
          $business_plan->save();


          \DB::table('comments')->insert([
              'user_id' => $business_plan->user_id,
              'post_id' => $business_plan->id,
              'comment' => $request->get('comments'),
              'commentor_id' => $commentor->id
          ]);



//          $comments = Comments::where('post_id', '=',$id)->first();
//          $comments->post_id = $id;
//          $comments->comment = $request->get('comments');
//          $comments->save();

          return redirect('admin/uploads/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          $doc = BusinessPlan::findOrFail($id);
          $file = $doc->doc_name;
          $filename = base_path() . '/storage/business_plans/' . $file;
          \File::delete($filename);
//          BusinessPlan::destroy($id->id);
          \DB::table('business_plan')->where('id', '=', $id)->delete();
          \DB::table('comments')->where('post_id', $id)->delete();
          return redirect('admin/uploads/index')->with('status', 'Upload Deleted Successfully!');
     }

}
