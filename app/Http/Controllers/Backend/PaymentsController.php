<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;

class PaymentsController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $payments = \DB::table('payment')
                  ->where([
                      ['payment.status2', 1],
                      ['payment.status', 'COMPLETED'],
                      ['payment.trackingid', '!=', NULL]
                  ])
                  ->join('users', 'users.id', '=', 'payment.user_id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'payment.amount', 'payment.id', 'payment.user_id', 'users.id_number')
                  ->paginate(10);


          $sum = Payment::with('users')
                  ->where([
                      ['payment.status2', TRUE],
                      ['payment.status', 'COMPLETED'],
                      ['payment.trackingid', '!=', NULL]
                  ])
                  ->sum('payment.amount');

//          $sum = \DB::table('payment')
//                  ->select(\DB::raw('user_id'), \DB::raw('sum(amount)'))
//                  ->groupBy(\DB::raw('user_id)'))
//                  ->get();

          return view('backend.payments.index', ['payments' => $payments, 'sum' => $sum]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          //
     }

     public function inactive() {
          $inactive = \DB::table('payment')
                  ->where([
                      ['payment.status', 'COMPLETED'],
                      ['payment.status2', FALSE],
                      ['payment.trackingid', '!=', NULL]
                  ])
                  
                  ->join('users', 'users.id', '=', 'payment.user_id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'payment.amount', 'payment.user_id', 'users.id_number')
                  ->paginate(10);

          $sum = Payment::with('users')
                  ->where([
                      ['payment.status2', FALSE],
                      ['payment.status', 'COMPLETED'],
                      ['payment.trackingid', '!=', NULL]
                  ])
                  ->sum('payment.amount');
          return view('backend.payments.inactive', ['inactive' => $inactive, 'sum' => $sum]);
     }

     public function pending() {
          $pending = \DB::table('payment')
                  ->where([
                      ['payment.status', 'PENDING']
                  ])
                  ->join('users', 'users.id', '=', 'payment.user_id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'payment.amount', 'payment.user_id', 'users.id_number', 'payment.id')
                  ->get();

          $sum = Payment::with('users')
                  ->where([
                      ['payment.status', 'PENDING']
                  ])
                  ->sum('payment.amount');
          return view('backend.payments.pending', ['pending' => $pending, 'sum' => $sum]);
     }

     public function invalid() {
          $invalid = \DB::table('payment')
                  ->where([
                      ['payment.status', 'INVALID']
                  ])
                  ->join('users', 'users.id', '=', 'payment.user_id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'payment.amount', 'payment.user_id', 'users.id_number')
                  ->get();

          $sum = Payment::with('users')
                  ->where([
                      ['payment.status', 'INVALID']
                  ])
                  ->sum('amount');
          return view('backend.payments.invalid', ['invalid' => $invalid, 'sum' => $sum]);
     }

     public function failed() {
          $failed = \DB::table('payment')
                  ->where([
                      ['payment.status', 'FAILED']
                  ])
                  ->join('users', 'users.id', '=', 'payment.user_id')
                  ->select('users.first_name', 'users.last_name', 'users.email', 'payment.amount', 'payment.user_id', 'users.id_number')
                  ->get();

          $sum = Payment::with('users')
                  ->where([
                      ['payment.status', 'FAILED']
                  ])
                  ->sum('amount');
          return view('backend.payments.failed', ['failed' => $failed, 'sum' => $sum]);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
          //
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          //
     }

}
