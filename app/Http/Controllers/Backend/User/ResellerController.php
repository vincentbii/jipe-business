<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\User;

class ResellerController extends Controller
{
	public function index(){
		return view('backend.resellers.withRequest');
	}

    public function CommWithdrawalRequest(){
    	$requests = \DB::table('comm_with_request')
    		->join('users', 'comm_with_request.user_id', '=', 'users.id')
    		->where('comm_with_request.status', TRUE)
    		->select('comm_with_request.*', 'users.first_name', 'users.last_name');

        return Datatables::of($requests)
            ->addColumn('action', function ($request) {
                return '<button class="edit-modal btn btn-info" data-id="{{'.$request->id.'}}" data-name="{{'.$request->first_name.'}}">
                <span class="glyphicon glyphicon-edit"></span> Edit</button>';
            })
            ->addColumn('mergeColumn', function($row){
      return $row->first_name.' '.$row->last_name;
})
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }
}
