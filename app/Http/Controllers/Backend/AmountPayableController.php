<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AmountPayableController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $amount = \DB::table('amount_to_be_payed')
                  ->where('status', TRUE)
                  ->first();
          return view('backend.amountPayable.index', ['amount' => $amount]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
          \DB::table('amount_to_be_payed')
                  ->insert([
                      'amount' => $request->get('amount')
          ]);


          return redirect('admin/amountpayable')->with('status', 'Inserted successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request) {
          \DB::table('amount_to_be_payed')
                  ->where('id', $request->get('id'))
                  ->update([
                      'amount' => $request->get('amount')
          ]);

          return redirect('admin/amountpayable')->with('status', 'Updated successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          //
     }

}
