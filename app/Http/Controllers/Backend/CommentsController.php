<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Backend\CommentsRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Mail;
use App\Models\Access\User\User;
use App\Models\Comments;
use Illuminate\Http\Request;

class CommentsController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index($id) {
          
          
          $proposal = \DB::table('business_plan')
                  ->where('id', $id)
                  ->first();
          
          $users = \DB::table('users')
                  ->where('id', '=', $proposal->user_id)
                  ->get();

          $comments = \DB::table('comments')
                  ->where('post_id', '=', $proposal->id)
                  ->join('users', 'users.id', '=', 'comments.commentor_id')
                  ->select('users.first_name', 'users.last_name', 'comments.id', 'comments.comment', 'comments.commentor_id')
                  ->get();

//          $commentor = \DB::table('users')
//                  ->where('user_id', '=', $id)
//                  ->join('comments', 'users.id', '=', 'comments.user_id')
//                  ->select('users.*', 'comments.commentor_id')
//                  ->get();
          return view('backend.comments.index2', ['comments' => $comments,
              'users' => $users,
              'proposal' => $proposal
              ]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create($id) {
          $proposal = \DB::table('business_plan')->where('id', $id)->first();
          return view('backend.comments.create', ['proposal' => $proposal]);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(CommentsRequest $request) {
          try {
               $commentor = Auth::user();
               
               $comment_by = \DB::table('users')
                       ->where('id', $commentor->id)
                       ->first();

               $user = User::findOrFail($request->get('user_id'));
               
               

               $status = \DB::table('comments')->insert([
                   'commentor_id' => $commentor->id,
                   'post_id' => $request->get('post_id'),
                   'user_id' => $request->get('user_id'),
                   'comment' => $request->get('comment')
                       ]
               );
               $comment = \DB::table('comments')->where('post_id', $request->get('post_id'))->first();
               
               if ($status) {
                    Mail::send('comment', ['comment' => $comment, 'user' => $user, 'comment_by' => $comment_by], function ($m) use ($user) {
                         $m->from('info@jipebusiness.co.ke', 'Jipe Business Limited');
                         $m->to($user->email, $user->name)->subject('Notification!');
                    });
                    return redirect('admin/uploads/index')->with('status', 'Your comment has been saved succefully!');
               }
          } catch (Exception $ex) {
               
          }
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $comment = Comments::findOrFail($id);
          return view('backend.comments.edit', compact('comment'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          
          $country = Comments::find($id);
          $country->comment = $request->get('comment');
          $country->save();

//          Session::flash('message', 'Successfully updated County!');
          return redirect('admin/uploads/index')->with('status', 'Updated Successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          //
     }

}
