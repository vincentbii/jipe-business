<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\EducationRequest;
use App\Http\Controllers\Controller;
use App\Models\Education;

class EducationController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $education_level = \DB::table('education_level')->get();
          return view('backend.education.index', ['education_level' => $education_level]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          return view('backend.education.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(EducationRequest $request) {
          $education_level = new Education;
          $education_level->name = $request->get('name');
          $education_level->save();

          return redirect('admin/education/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          $education_level = Education::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.education.edit', compact('education_level'));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $education_level = Education::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.education.edit', compact('education_level'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(EducationRequest $request, $id) {
          $education_level = Education::find($id);
          $education_level->id = $request->get('id');
          $education_level->name = $request->get('name');
//            $nerd->nerd_level = Input::get('nerd_level');
          $education_level->save();

//          Session::flash('message', 'Successfully updated County!');
          return redirect('admin/education/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('education_level')->where('id', '=', $id)->delete();
          return back()->withInput();
     }

}
