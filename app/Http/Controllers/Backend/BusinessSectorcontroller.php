<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\BusinessSectorRequest;
use App\Http\Controllers\Controller;

class BusinessSectorcontroller extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $business_sectors = \DB::table('business_sector')
                  ->where('status', 1)
                  ->get();
          return view('backend.businesssector.index', ['business_sectors' => $business_sectors]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          return view('backend.businesssector.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(BusinessSectorRequest $request) {
          \DB::table('business_sector')->insert([
              'name' => $request->get('name')
          ]);
          return redirect('admin/businesssector')->with('status', 'Saved successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('business_sector')->where('id', '=', $id)->delete();
          return redirect('admin/businesssector')->with('status', 'Deleted successfully');
     }

}
