<?php

namespace App\Http\Controllers\Backend;

use App\Models\County;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CountyRequest;

class CountyController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $counties = \DB::table('county')->get();
          return view('backend.county.index', ['counties' => $counties]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          return view('backend.county.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(CountyRequest $request) {
          $counties = new County;
          $counties->name = $request->get('name');
          $counties->save();

          return redirect('admin/county/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $county = County::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.county.edit', compact('county'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(CountyRequest $request, $id) {
          $county = County::find($id);
          $county->id = $request->get('id');
          $county->name = $request->get('name');
//            $nerd->nerd_level = Input::get('nerd_level');
          $county->save();

//          Session::flash('message', 'Successfully updated County!');
          return redirect('admin/county/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('county')->where('id', '=', $id)->delete();
          return back()->withInput();
     }

}
