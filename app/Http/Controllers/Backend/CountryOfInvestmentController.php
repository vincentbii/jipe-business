<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryOfInvestmentController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $countriesofinvestment = \DB::table('countryofinvestment')
                  ->where('country.status', 1)
                  ->join('country', 'country.id', '=', 'countryofinvestment.country_id')
                  ->select('country.country_name', 'countryofinvestment.country_id', 'countryofinvestment.id')
                  ->get();
          return view('backend.countryofinvestment.index', ['countriesofinvestment' => $countriesofinvestment]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          $countries = \DB::table('country')->get();
          return view('backend.countryofinvestment.create', ['countries' => $countries]);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request) {
//          $value = implode(',', $request->get('country_id'));
          foreach ($request->get('country_id') as $value) {
               \DB::table('countryofinvestment')->insert(
                       ['country_id' => $value]
               );
          }
//          $countryofinvestment = implode(",", $request->get('country_id'));
//
//          $status = \DB::table('countryofinvestment')->insert(
//                  ['country_id' => $countryofinvestment]
//          );
//          if ($status) {
//               \Session::flash('flash_message', 'created!!');
//          } else {
//               \Session::flash('flash_message', 'Error!');
//          }

          return redirect('admin/countryofinvestment/index')->with('status', 'Saved successfully');

//          return redirect('backend.countryofinvestment.index');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id) {
          //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('countryofinvestment')->where('id', '=', $id)->delete();
          return redirect('admin/countryofinvestment/index')->with('status', 'Saved successfully');
//          return $id;
     }

}
