<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CountryRequest;
use App\Http\Controllers\Controller;
use App\Models\Country;

class CountryController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $countries = \DB::table('country')->get();
          return view('backend.country.index', ['countries' => $countries]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          return view('backend.country.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(CountryRequest $request) {
          $countries = new Country;
          $countries->country_name = $request->get('country_name');
          $countries->save();

          return redirect('admin/country/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $country = Country::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.country.edit', compact('country'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(CountryRequest $request, $id) {
          $country = Country::find($id);
          $country->country_name = $request->get('country_name');
          $country->save();

//          Session::flash('message', 'Successfully updated County!');
          return redirect('admin/country/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
//          $county = Country::findOrFail($id);
//          return $county->id;
          \DB::table('country')->where('id', '=', $id)->delete();
          return back()->withInput();
     }

}
