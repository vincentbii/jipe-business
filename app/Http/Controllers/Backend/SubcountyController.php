<?php

namespace App\Http\Controllers\Backend;

use App\Models\County;
use App\Models\Subcounty;
use App\Http\Requests\Backend\SubcountyRequest;
use App\Http\Controllers\Controller;

class SubcountyController extends Controller {

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index() {
          $sub_county = \DB::table('subcounty')->get();
          return view('backend.subcounty.index', ['sub_county' => $sub_county]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create() {
          $counties = County::all();
          return view('backend.subcounty.create', compact('counties'));
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(SubcountyRequest $request) {
          $sub_counties = new Subcounty;
          $sub_counties->county_id = $request->get('county_id');
          $sub_counties->name = $request->get('name');
          $sub_counties->save();

          return redirect('admin/subcounty/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id) {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id) {
          $sub_county = Subcounty::findOrFail($id);
//          return $county->id;
//          return view('county.edit', compact('county'));
          return view('backend.subcounty.edit', compact('sub_county'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(SubcountyRequest $request, $id) {
          $sub_county = Subcounty::find($id);
          $sub_county->county_id = $request->get('county_id');
          $sub_county->id = $request->get('id');
          $sub_county->name = $request->get('name');
//            $nerd->nerd_level = Input::get('nerd_level');
          $sub_county->save();

//          Session::flash('message', 'Successfully updated County!');
          return redirect('admin/subcounty/index')->with('message', 'Your Entry saved successfully');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id) {
          \DB::table('subcounty')->where('id', '=', $id)->delete();
          return back()->withInput();
     }

}
