<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    public function affiliate($affcode){
    	if( \Invite::isValid($affcode)){
            $invitation = \Invite::get($affcode); //retrieve invitation modal
            $invited_email = $invitation->email;
            $referral_user = $invitation->user;
            // return redirect('register');
            return redirect()->route('frontend.auth.registeraff', ['id' => $affcode]);

    // show signup form
} else {
    $status = \Invite::status($affcode);
    // show error or show simple signup form
    return $status;
}

    }

}
