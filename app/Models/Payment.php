<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

     protected $table = 'payment';
     public $timestamps = false;
     
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
