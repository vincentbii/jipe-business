<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryOfInvestment extends Model {

     protected $table = 'countryofinvestment';
     public $timestamps = false;

}
