<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessPlan extends Model {

     //
     protected $table = 'business_plan';
     public $timestamps = false;
     
     public function comments() {
          return $this->hasMany('App\Models\Comments');
     }
     

}
