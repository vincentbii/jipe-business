<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class county extends Model {

     protected $table = 'county';
     public $timestamps = false;

     public function id() {
          return $this->hasMany('App\Models\Subcounty');
     }

}
