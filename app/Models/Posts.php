<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

     /**
      * The database table used by the model.
      *
      * @var string
      */
     protected $table = 'comments';

     /**
      * Define guarded columns
      *
      * @var array
      */
     protected $guarded = array('id');

}
