<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Junaidnasir\Larainvite\InviteTrait;

class User extends Model {
	use InviteTrait;

     protected $table = 'users';

     public function payment() {
          return $this->hasMany('App\Models\Payment');
     }

}
