<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Financed extends Model
{
     protected $table = 'financed';
     public $timestamps = false;
}
