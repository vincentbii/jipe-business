<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmountPayable extends Model
{
     protected $table = 'amount_to_be_payed';
     public $timestamps = false;
}
