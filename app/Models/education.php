<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class education extends Model
{
    protected $table = 'education_level';
     public $timestamps = false;
}
