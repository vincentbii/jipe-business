<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessSector extends Model {

     protected $table = 'business_sector';
     public $timestamps = false;

}
