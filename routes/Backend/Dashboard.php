<?php

Route::get('manage-payment', 'PaymentResultscontroller@manageVue');
Route::resource('managepayment','PaymentResultscontroller');

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::get('uploads/index', 'BusinessPlanController@index')->name('uploads');
Route::get('uploads/preview/{id}', 'BusinessPlanController@preview')->name('preview');
Route::delete('uploads/destroy/{id}', 'BusinessPlanController@destroy')->name('destroy');
Route::patch('uploads/update/{id}', 'BusinessPlanController@update')->name('update');
Route::post('uploads/finance', 'BusinessPlanController@finance')->name('finance');
Route::get('uploads/approve/{id}', 'BusinessPlanController@approve')->name('approve');
Route::get('uploads/deactivate/{id}', 'BusinessPlanController@deactivate')->name('deactivate');
Route::get('uploads/download/{id}', 'BusinessPlanController@download')->name('download');
Route::get('uploads/view/{id}', 'BusinessPlanController@view')->name('view');

Route::get('comments/index/{id}', 'CommentsController@index')->name('index');
Route::get('comments/create/{id}', 'CommentsController@create')->name('create');
Route::post('comments/store', 'CommentsController@store')->name('store');
Route::delete('comments/destroy/{id}', 'CommentsController@destroy')->name('admincommentsdestroy');
Route::get('comments/edit/{id}', 'CommentsController@edit')->name('admin-comment-edit');
Route::patch('comments/update/{id}', 'CommentsController@update')->name('commentsupdate');

//Amount to be payed; default: KES. 1000

Route::get('amountpayable', 'AmountPayableController@index')->name('amountpayable');
Route::post('amountpayable/store', 'AmountPayableController@store');
Route::post('amountpayable/update', 'AmountPayableController@update');

Route::resource('financed_amount', 'FinancedAmountController');

Route::get('financedAmount', 'FinancedAmountController@financedAmount')->name('financedAmount');



//Show payments
Route::get('payments/inactive', 'PaymentsController@inactive')->name('paymentsinactive');
Route::get('payments/index', ['as' => 'paymentsindex','uses' => 'PaymentsController@index']);
Route::get('payments/invalid', 'PaymentsController@invalid');
Route::get('payments/pending', 'PaymentsController@pending');
Route::get('payments/failed', 'PaymentsController@failed');
Route::resource('payments', 'PaymentsController');
Route::get('managePayments', 'PaymentsController@managePayments');


//countryOfInvestment
//Route::resource('countryofinvestment', 'CountryOfInvestmentController');
Route::get('countryofinvestment/index', 'CountryOfInvestmentController@index')->name('countryofinvestment');
Route::get('countryofinvestment/create', 'CountryOfInvestmentController@create')->name('countryofinvestmentcreate');
Route::patch('countryofinvestment/update', 'CountryOfInvestmentController@update')->name('countryofinvestmentupdate');
Route::delete('countryofinvestment/destroy/{id}', 'CountryOfInvestmentController@destroy')->name('countryofinvestmentdestroy');
Route::get('countryofinvestment/edit', 'CountryOfInvestmentController@edit')->name('countryofinvestmentedit');
Route::post('countryofinvestment/store', 'CountryOfInvestmentController@store')->name('countryofinvestmentstore');

//business sector
Route::resource('businesssector', 'BusinessSectorController');

//payment method
Route::resource('paymentmethod', 'PaymentMethodController');



Route::get('uploads', 'BusinessPlanController@index')->name('uploads');
Route::get('uploads/index', 'BusinessPlanController@index')->name('uploads.index');
Route::get('uploads/financed', 'BusinessPlanController@financed')->name('uploads.financed');
Route::get('uploads/approved', 'BusinessPlanController@approved')->name('uploads.approved');
Route::get('uploads/deactivated', 'BusinessPlanController@deactivated')->name('uploads.deactivated');
Route::get('uploads/deleted', 'BusinessPlanController@deleted')->name('uploads.deleted');
Route::get('uploads/disapprove/{id}', 'BusinessPlanController@disapprove')->name('uploads.disapprove');
Route::get('uploads/unfinance/{id}', 'BusinessPlanController@unfinance')->name('uploads.unfinance');
Route::get('uploads/activate/{id}', 'BusinessPlanController@activate')->name('uploads.activate');
Route::group([
    'namespace' => 'User',
    'as' => 'reseller.',
    'middleware' => 'access.routeNeedsRole:1',
], function(){
    Route::get('CommWithdrawalRequest', 'ResellerController@CommWithdrawalRequest')->name('CommWithdrawalRequest');

    Route::get('requestsIndex', 'ResellerController@index')->name('requestsIndex');
});