<?php
Route::get('country/index', 'CountryController@index')->name('country');
Route::get('country/create', 'CountryController@create')->name('countrycreate');
Route::post('country/store', 'CountryController@store')->name('countrystore');
Route::delete('country/destroy/{id}', 'CountryController@destroy')->name('countrydestroy');
Route::get('country/edit/{id}', 'CountryController@edit')->name('countryedit');
Route::patch('country/update/{id}', 'CountryController@update')->name('countryupdate');

Route::get('county/index', 'CountyController@index')->name('county');
Route::get('county/create', 'CountyController@create')->name('countycreate');
Route::post('county/store', 'CountyController@store')->name('countystore');
Route::delete('county/destroy/{id}', 'CountyController@destroy')->name('countydestroy');
Route::get('county/edit/{id}', 'CountyController@edit')->name('countyedit');
Route::patch('county/update/{id}', 'CountyController@update')->name('countyupdate');

Route::post('subcounty/store', 'SubcountyController@store');
Route::get('subcounty/index', 'SubcountyController@index');
Route::get('subcounty/create', 'SubcountyController@create');
Route::delete('subcounty/destroy/{id}', 'SubcountyController@destroy');
Route::get('subcounty/edit/{id}', 'SubcountyController@edit');
Route::patch('subcounty/update/{id}', 'SubcountyController@update');

Route::get('education/index', 'EducationController@index')->name('education');
Route::get('education/create', 'EducationController@create')->name('educationcreate');
Route::post('education/store', 'EducationController@store')->name('educationstore');
Route::delete('education/destroy/{id}', 'EducationController@destroy')->name('educationdestroy');
Route::get('education/edit/{id}', 'EducationController@edit')->name('educationedit');
Route::patch('education/update/{id}', 'EducationController@update')->name('educationupdate');

Route::get('uploads/finance/{id}', 'BusinessPlanController@finance');


Route::group([
    'as'     => 'settings.',
    'namespace' => 'Settings',
    'middleware' => 'access.routeNeedsRole:1',
], function(){
    Route::resource('affCommission', 'affCommissionController');
    Route::resource('subscriptions', 'PaymentResultscontroller');
});


/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix'     => 'access',
    'as'         => 'access.',
    'namespace'  => 'Access',
], function () {

    /*
     * User Management
     */
    Route::group([
        'middleware' => 'access.routeNeedsRole:1',
    ], function () {
        Route::group(['namespace' => 'User'], function () {
            /*
             * For DataTables
             */
            Route::post('user/get', 'UserTableController')->name('user.get');

            /*
             * User Status'
             */
            Route::get('user/deactivated', 'UserStatusController@getDeactivated')->name('user.deactivated');
            Route::get('user/deleted', 'UserStatusController@getDeleted')->name('user.deleted');

            /*
             * User CRUD
             */
            Route::resource('user', 'UserController');

            /*
             * Specific User
             */
            Route::group(['prefix' => 'user/{user}'], function () {
                // Account
                Route::get('account/confirm/resend', 'UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');

                // Status
                Route::get('mark/{status}', 'UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);

                // Password
                Route::get('password/change', 'UserPasswordController@edit')->name('user.change-password');
                Route::patch('password/change', 'UserPasswordController@update')->name('user.change-password.post');

                // Access
                Route::get('login-as', 'UserAccessController@loginAs')->name('user.login-as');

                // Session
                Route::get('clear-session', 'UserSessionController@clearSession')->name('user.clear-session');
            });

            /*
             * Deleted User
             */
            Route::group(['prefix' => 'user/{deletedUser}'], function () {
                Route::get('delete', 'UserStatusController@delete')->name('user.delete-permanently');
                Route::get('restore', 'UserStatusController@restore')->name('user.restore');
            });
        });

        /*
        * Role Management
        */
        Route::group(['namespace' => 'Role'], function () {
            Route::resource('role', 'RoleController', ['except' => ['show']]);

            //For DataTables
            Route::post('role/get', 'RoleTableController')->name('role.get');
        });
    });
});
