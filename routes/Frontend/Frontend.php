<?php
Route::get('registertrial', 'Frontendcontroller@registertrial')->name('registertrial');
Route::get('about', 'FrontendController@about')->name('about');
Route::get('grantprogram', 'FrontendController@grant')->name('grantprogram');
Route::get('contact', 'FrontendController@contact')->name('contact');
Route::post('contactus', 'FrontendController@contactus')->name('contactus');


/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::get('macros', 'FrontendController@macros')->name('macros');

Route::get('/re', function () {
     $county_id = Input::get('county_id');

     $sub_county_id = Subcounty::where('county_id', '=', $county_id)->get();

     return Response::json($sub_county_id);
});

Route::get('/registerr', function () {
     $county = County::all();

     return view('frontend.auth.register')->with('county', $county);
});

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
     Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
      Route::get('resellers', 'ResellerController@reseller')->name('resellers');
      Route::post('activateReseller', 'ResellerController@activateReseller')->name('activateReseller');

      Route::post('requestWith', 'ResellerController@requestWith')->name('requestWith');


//          Route::get('donepayment', 'PaymentsController@paymentsuccess')->name('paymentsuccess');
          
          Route::post('transact', 'PaymentsController@payment')->name('payment');
          Route::get('makepayment', 'PaymentsController@makePayment')->name('makepayment');

          // Route::get('paymentsuccess', 'PaymentsController@paymentsuccess')->name('paymentsuccess');
//          Route::post('storepayment', 'PaymentsController@storePayment')->name('storepayment');
        
          
          /*
           * User Dashboard Specific
           */
          Route::get('dashboard', 'DashboardController@index')->name('dashboard');
          Route::get('dashboard1', 'PaymentsController@dashboard')->name('dashboard1');
          /*
           * User Account Specific
           */
          Route::get('comments/index/{id}', 'CommentsController@index');
          Route::get('comments/create', 'CommentsController@create')->name('commentscreate');
          Route::post('comments/store', 'CommentsController@store')->name('commentsstore');
          Route::delete('comments/destroy/{id}', 'CommentsController@destroy');
          
          //Route JQuery
          Route::get('comments/jquery', 'CommentsController@jquery');


          Route::post('register/store', 'RegisterController@showRegistrationForm');

          Route::get('account', 'AccountController@index')->name('account');

          //Business Plan Upload
          Route::delete('upload/destroy/{id}', 'BusinessPlanController@destroy');

          Route::get('upload', 'BusinessPlanController@index')->name('upload');
          Route::PATCH('upload/update/{id}', 'BusinessPlanController@update')->name('update');
          Route::post('store', 'BusinessPlanController@store')->name('store');
          Route::get('create', 'BusinessPlanController@create')->name('create');
          Route::post('destroy/{$id}', 'BusinessPlanController@destroy')->name('destroy');
          Route::get('upload/download/{id}', 'BusinessPlanController@download');
          Route::get('upload/preview/{id}', 'BusinessPlanController@preview');
          Route::get('upload/updatedescription/{id}', 'BusinessPlanController@updatedescription');
          Route::get('upload/uploadproposal', 'BusinessPlanController@uploadproposal');
          
          
          Route::get('upload/proposals', 'BusinessPlanController@businessProposals');
          /*
           * User Profile Specific
           */
          Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
     });
});
